
#ifndef SCREEN_H
#define SCREEN_H

#include "windows.h"

// This file contains information regarding the following...
//  - Window Creation
//  - Window Update
//  - Window Redraw
//  - Window Resize
//  - Window Deletion


// This structure contains information regarding a Micrsoft Windows' window
struct MS_Window
{
    WNDCLASS wc;
    HWND hwnd;
}






MS_Window CreateWindowImage();
int UpdateWindowImage();
int RedrawWindowImage();
int ResizeWindowImage();
int DeleteWindowImage();


#endif // SCREEN_H

