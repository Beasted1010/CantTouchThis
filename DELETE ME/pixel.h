

#ifndef PIXEL_H
#define PIXEL_H

#include "stdint.h"
#include "bitmap.h"
#include "common.h"

typedef struct Point_px
{
    uint32_t x;
    uint32_t y;
} Point_px;

typedef struct Line_px
{
    Point_px p1;
    Point_px p2;
} Line_px;

typedef struct Triangle_px
{
    Point_px tip_point;
    Line_px base_line;
} Triangle_px;

typedef struct Rect_px
{
    uint32_t left;
    uint32_t top;
    uint32_t right;
    uint32_t bottom;
} Rect_px;

typedef struct Circle_px
{
    uint32_t radius;
    Point_px center;
} Circle_px;

typedef struct Texture
{
    unsigned char* image_data;
    BITMAPINFO* bmi;
    Rect_px rect;

    uint32_t width;
    uint32_t height;

    char* file_location;
} Texture;


void ColorPixels(Bitmap* bitmap, RGB* rgb);
void DrawQuarterCircle(Bitmap* bitmap, Circle_px* circle, Quadrant quadrant, RGB* color);
void DrawCircle(Bitmap* bitmap, Circle_px* circle, RGB* color);
void DrawRepeatSymbol(Bitmap* bitmap, Circle_px* circle, uint32_t thickness, RGB* color);
void DrawLine(Bitmap* bitmap, Line_px* line, RGB* color);
void DrawHorizontalLine(Bitmap* bitmap, uint32_t start_x, uint32_t start_y, uint32_t end_x, RGB* color);
void DrawVerticalLine(Bitmap* bitmap, uint32_t start_x, uint32_t start_y, uint32_t end_y, RGB* color);
void DrawTriangle(Bitmap* bitmap, Triangle_px* triangle, RGB* color);
void FillTriangle(Bitmap* bitmap, Triangle_px* triangle, RGB* color);
void DrawRectangle(Bitmap* bitmap, Rect_px* rect, RGB* color);
void FillRectangle(Bitmap* bitmap, Rect_px* rect, RGB* color);

Texture CreateTexture(char* file_location);
void DestroyTexture(Texture* texture);
void PlaceTexture(Bitmap* bitmap, Texture* texture, uint32_t x_pos, uint32_t y_pos);

#endif //PIXEL_H
