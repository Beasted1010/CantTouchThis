
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "pixel.h"
#include <stdint.h>

#define SCREEN_WIDTH_METERS 16
#define SCREEN_HEIGHT_METERS 9

//#define MAX_ACCELERATION .1
//#define MAX_VELOCITY 0.01
#define MAX_ACCELERATION 0.75
#define MAX_VELOCITY 1

#define MIN_ENTITY_WIDTH 0.1
#define MAX_ENTITY_WIDTH 0.25

#define MIN_ENTITY_HEIGHT 0.1
#define MAX_ENTITY_HEIGHT 0.25

#define MIN_ENTITY_HEALTH 40
#define MAX_ENTITY_HEALTH 100

#define FRAMES_PER_SECOND 60

#define SPAWN_RATE .5
#define SPAWN_FRAME_BASE 50
#define INITIAL_ENEMY_COUNT 3

#define PROXIMITY_DEMO 6
#define PROXIMITY_EASY 4
#define PROXIMITY_MEDIUM 3
#define PROXIMITY_HARD 2
#define PROXIMITY_IMPOSSIBLE 1

//#define CONSTANT_ACCEL .25
#define CONSTANT_ACCEL .5

typedef enum PlayerKeys
{
    KeyUp,
    KeyDown,
    KeyLeft,
    KeyRight,
    KeyNum
} PlayerKeys;

typedef struct Input
{
    uint8_t keydown[KeyNum];
    uint8_t mousestate;
    uint16_t mousex, mousey;
} Input;

typedef enum Difficulty
{
    Demo,
    Easy,
    Medium,
    Hard,
    Impossible
} Difficulty;

typedef enum CollisionCorner
{
    TOP_LEFT_INTO_BOTTOM_RIGHT = 1,
    TOP_RIGHT_INTO_BOTTOM_LEFT,
    BOTTOM_LEFT_INTO_TOP_RIGHT,
    BOTTOM_RIGHT_INTO_TOP_LEFT,
} CollisionCorner;

typedef struct Point
{
    float x_meter;
    float y_meter;
} Point;

typedef struct Line
{
    Point p1;
    Point p2;
    float slope;
    float y_intercept;
} Line;

typedef struct Circle
{
    Point center;
    float radius;
} Circle;

typedef struct Rect
{
    float x_meter;
    float y_meter;
    float width;
    float height;
} Rect;

typedef struct Size
{
    float width;
    float height;
} Size;

typedef struct Physics
{
    float accel_x;
    float accel_y;
    float vel_x;
    float vel_y;
} Physics;

typedef struct Entity
{
    Point coords;
    Size size;

    uint8_t health;
    
    Physics physics;

    struct Texture tex;
} Entity;

typedef struct Player
{
    Input input;
    uint32_t score;
    
    Entity entity;
} Player;

typedef struct Enemy
{
    Entity entity;
} Enemy;

typedef struct Obstacle
{
    Entity entity;
} Obstacle;

typedef struct GameState
{
    uint8_t game_over;
    uint8_t running;
    uint8_t paused;

    Difficulty difficulty;
    
    uint64_t logical_frames;
    uint32_t fps;

    float m_to_px_conversion_factor_x;
    float m_to_px_conversion_factor_y;

    Player player;
    Enemy* enemies;
    Obstacle* obstacles;

    uint32_t num_enemies;
    uint32_t num_obstacles;
    
} GameState;

void InitializeGameState(GameState* state);
void UpdateGameState(GameState* state);
void DestroyGameState(GameState* state);
Rect GetEntityRectangle(Entity* entity);

Circle GetProximityCircle(Player* player, Difficulty difficulty);

#endif //GAMESTATE_H
