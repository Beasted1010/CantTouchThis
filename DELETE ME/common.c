

#include "common.h"


void ValidateObject(void* ptr, char* name)
{
    if(!ptr)
    {
        printf("Failed to allocate memory for %s!\n", name);
        // TODO: Failure stuff
    }
}

float AbsoluteValue( float val )
{
    if( val >= 0 )
        return val;
    else
        return -val;
}

float Exponentiate( float base, float exponent )
{
    int i;
    float result = 1;
    for( i = 0; i < exponent; i++ )
    { result *= base; }

    return result;
}

// Newton's method
float NthRoot( int nth_root, float val )
{
    float threshold = 0.001;
    //printf("nth_root = %i\nthreshold = %f\n\n", threshold);

    int num_iterations = 0;

    // TODO: NEED TO CALCULATE THIS, PERHAPS WITH SOME SORT OF SQUEEZE LIMIT THINGY? estimate.
    float guess = val / 2.0;
    int converged = 0;
    float f_x;
    float f_prime_x;

    float old_guess = guess;

    while(!converged)
    {
        f_x = Exponentiate(guess, nth_root) - val; // Function x^n - i = 0, from x = nth_root(i)
        f_prime_x = nth_root * Exponentiate(guess, nth_root - 1); // Power rule -> n * (x^(n-1))
        //printf("f_x = %f\nf_prime_x = %f\n\n", f_x, f_prime_x);

        old_guess = guess;
        guess = guess - (f_x / f_prime_x); // Update guess
        //printf("old_guess = %f\nnew guess = %f\n\n", old_guess, guess);

        num_iterations++;

        //printf("Change = %f\n", AbsoluteValue(old_guess - guess));
        if( AbsoluteValue(old_guess - guess) < threshold)
            converged = 1;
    }

    //printf("%i root of %f = %f", nth_root, val, guess);
    //printf("Calculation took %i iterations\n", num_iterations);

    return guess;
}












