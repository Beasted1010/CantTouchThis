

#include "windows.h"

#include "common.h"
#include "gamestate.h"
#include "bitmap.h"
#include "pixel.h"
#include "screen.h"

#include "stdio.h"
#include "stdint.h"

GameState state;

struct MS_Window wnd;

void HandlePaintEvent(HWND hwnd, HDC hdc);

// hwnd: Handle to the window
// uMsg: The message code (e.g. WM_SIZE message which indicates the window was resized)
// wParam and lParam: Additional data pertaining to the message -> exact meaning depends on message code (uMsg)
// LRESULT is an integer value that your program returns to Windows, it contains the programs'r esponse to the message
//      The exact meaning of this return value depends on the message code (uMsg)
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        // REGION: Window
        case WM_CREATE:
        {

        } break;

        case WM_SIZE:
        {
            if(!ResizeMSWindow(&wnd))
            {
                printf("Failed to resize the window! Error: %lx\n", GetLastError());
            }
            state.m_to_px_conversion_factor_x = wnd.image.w / SCREEN_WIDTH_METERS;
            state.m_to_px_conversion_factor_y = wnd.image.h / SCREEN_HEIGHT_METERS;
        } break;

        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* min_max_info = (MINMAXINFO*) lParam;

            min_max_info->ptMinTrackSize.x = MIN_SCREEN_WIDTH_PX;
            min_max_info->ptMinTrackSize.y = MIN_SCREEN_HEIGHT_PX;
        } break;

        // Triggered by hitting close button (red X) or ALT-F4, and any other closing actions
        case WM_CLOSE:
        {
            state.running = 0;
            DestroyMSWindow(&wnd); // Destroy the specified window 
            PostQuitMessage(0); // Send a quit message to have GetMessage return 0 (thus stop receiving new messages)
        } break;

        // Triggered after DestroyWindow (called by defalt for WM_CLOSE) and after window removed but before destruction
        case WM_DESTROY:
        {

        }

        // REGION: Paint
        // The message sent by OS when we must repaint the window
        // NOTE: OS handles painting of title bar and surrounding frame. The "Client Area" is our only responsibility
        // Only the "Update Region" (not whole client region) gets painted, this is the region that has had something change
        // Every time we paint to the update region, we clear the update region so that OS knows we are done with that area
        // E.g. if a user moves a window over our client area, then the update region is under that moved window
        //      The update region does not need to be redrawn since a window is now obstructing its view
        //      A similar situation happens when stretching the window
        // During the paint event you can either paint the entire client region or just the update region (your choice)
        //      Code is simpler if you just paint everything, but more efficient if you only paint update region
        //      The rcPaint member of PAINTSTRUCT will contain the region that needs updated
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            // All painting occurs between BeginPaint and EndPaint
            HDC hdc = BeginPaint(hwnd, &ps); // Begin painting region

            if( !hdc ) { printf("BeginPaint failed!\n"); }

            HandlePaintEvent(hwnd, hdc);

            EndPaint(hwnd, &ps); // End painting region -> Clears the update region
        } break;

        // REGION: INPUT
        case WM_LBUTTONDOWN:
        {

        } break;

        case WM_LBUTTONUP:
        {

        } break;

        case WM_RBUTTONDOWN:
        {

        } break;

        case WM_RBUTTONUP:
        {
            
        } break;

        case WM_KEYDOWN:
        {
            switch(wParam)
            {
                case 0x57: // W Key -> Up
                case VK_UP: // Up arrow key
                {
                    state.player.input.keydown[KeyUp] = 1;
                } break;

                case 0x41: // A Key -> Left
                case VK_LEFT: // Left arrow key
                {
                    state.player.input.keydown[KeyLeft] = 1;
                } break;

                case 0x53: // S Key -> Down
                case VK_DOWN: // Down arrow key
                {
                    state.player.input.keydown[KeyDown] = 1;
                } break;

                case 0x44: // D Key -> Right
                case VK_RIGHT: // Right arrow key
                {
                    state.player.input.keydown[KeyRight] = 1;
                } break;

                case VK_ESCAPE:
                {
                    state.paused = state.paused ? 0 : 1;
                } break;

                case 0x58: // X Key -> Exit if Paused
                {
                    if(state.paused) { state.running = 0; }
                } break;

                case 0x52: // R Key -> Restart if Paused
                {
                    if(state.paused) { DestroyGameState(&state); InitializeGameState(&state); }
                } break;
            }
        } break;

        case WM_KEYUP:
        {
            switch(wParam)
            {
                case 0x57: // W Key -> Up
                case VK_UP: // Up arrow key
                {
                    state.player.input.keydown[KeyUp] = 0;
                } break;

                case 0x41: // A Key -> Left
                case VK_LEFT: // Left arrow key
                {
                    state.player.input.keydown[KeyLeft] = 0;
                } break;

                case 0x53: // S Key -> Down
                case VK_DOWN: // Down arrow key
                {
                    state.player.input.keydown[KeyDown] = 0;
                } break;

                case 0x44: // D Key -> Right
                case VK_RIGHT: // Right arrow key
                {
                    state.player.input.keydown[KeyRight] = 0;
                } break;
            }
        } break;

        default:
        {
            return DefWindowProc(hwnd, uMsg, wParam, lParam); // Handle the message in its default manner
        } break;
    }
}

void DrawGameState()
{
    uint32_t player_loc_x_px = state.m_to_px_conversion_factor_x * state.player.entity.coords.x_meter;
    uint32_t player_loc_y_px = state.m_to_px_conversion_factor_y * state.player.entity.coords.y_meter;
    //printf("player x coords = %f\nplayer y coords = %f\n", state.player.entity.coords.x_meter, state.player.entity.coords.y_meter);
    //printf("player_loc_x_px = %i\nplayer_loc_y_px = %i\n", player_loc_x_px, player_loc_y_px);

    uint32_t player_width_px = state.m_to_px_conversion_factor_x * state.player.entity.size.width;
    uint32_t player_height_px = state.m_to_px_conversion_factor_y * state.player.entity.size.height;
    //printf("player_width_px = %i\nplayer_height_px = %i\n", player_width_px, player_height_px);

    struct RGB color = {.r=255, .g=0, .b=0, .a=0};

    struct xRect rect;
    rect.left = player_loc_x_px - (player_width_px / 2);
    rect.top = player_loc_y_px - (player_height_px / 2);
    rect.right = player_loc_x_px + (player_width_px / 2);
    rect.bottom = player_loc_y_px + (player_height_px / 2);
    //printf("Player top left = (%i,%i)\n", rect.left, rect.top);
    DrawRectangle(&wnd.image, &rect, &color);

    // If you want to draw the proximity circle that is around the player
    if( 0 )
    {
        struct RGB proximity_circle_color = {.r=0, .g=0, .b=255, .a=0};
        Circle circle_m = GetProximityCircle(&state.player, state.difficulty);
        struct xCircle proximity_circle = {.center.x = circle_m.center.x_meter * state.m_to_px_conversion_factor_x,
                                           .center.y = circle_m.center.y_meter * state.m_to_px_conversion_factor_y,
                                           .radius = circle_m.radius * state.m_to_px_conversion_factor_x};
        DrawCircle(&wnd.image, &proximity_circle, &proximity_circle_color);
    }

    for(int i = 0; i < state.num_enemies; i++)
    {
        uint32_t enemy_loc_x_px = state.m_to_px_conversion_factor_x * state.enemies[i].entity.coords.x_meter;
        uint32_t enemy_loc_y_px = state.m_to_px_conversion_factor_y * state.enemies[i].entity.coords.y_meter;
        //printf("enemy_loc_x_px = %i\nenemy_loc_y_px = %i\n", enemy_loc_x_px, enemy_loc_y_px);

        uint32_t enemy_width_px = state.m_to_px_conversion_factor_x * state.enemies[i].entity.size.width;
        uint32_t enemy_height_px = state.m_to_px_conversion_factor_y * state.enemies[i].entity.size.height;

        struct RGB color = {.r=0, .g=255, .b=0, .a=0};

        struct xRect rect;
        rect.left = enemy_loc_x_px - (enemy_width_px / 2);
        rect.top = enemy_loc_y_px  - (enemy_height_px / 2);
        rect.right = enemy_loc_x_px  + (enemy_width_px / 2);
        rect.bottom = enemy_loc_y_px + (enemy_height_px / 2);
        DrawRectangle(&wnd.image, &rect, &color);


        // TODO: Gamestate keep track of textures? I.e. Dynamic array of textures
    enemy_loc_x_px -= (state.enemies[i].entity.tex.rect.right - state.enemies[i].entity.tex.rect.left) / (float) 2.0;
    enemy_loc_y_px -= (state.enemies[i].entity.tex.rect.bottom - state.enemies[i].entity.tex.rect.top) / (float) 2.0;
    PlaceTexture(&state.enemies[i].entity.tex, &wnd.image, enemy_loc_x_px, enemy_loc_y_px, AlphaBinary);
    }

    if(state.paused)
    {
        struct RGB pause_color = {.r=255, .g=255, .b=255, .a=0};
        for(int i = 0; i < 2; i++)
        {
            struct xRect rect;
            rect.left = wnd.image.w - 3 * ( 10 * (i+1) );
            rect.top = 10;
            rect.right = rect.left + 10;
            rect.bottom = rect.top + 40;
            DrawRectangle(&wnd.image, &rect, &pause_color);
        }
    }

    if(state.game_over)
    {
        struct RGB end_game_color = {.r=255, .g=255, .b=255, .a=0};
        struct xRect end_game_rect;
        end_game_rect.left = (wnd.image.w / 2.0) - 50;
        end_game_rect.right = end_game_rect.left + 100;
        end_game_rect.top = (wnd.image.h / 2.0) - 50;
        end_game_rect.bottom = end_game_rect.top + 100;
        //FillRectangle(&wnd.image, &end_game_rect, &end_game_color);
        
        struct RGB repeat_symbol_color = {.r=255, .g=0, .b=0, .a=0};
        struct xCircle repeat_symbol;
        repeat_symbol.radius = ((end_game_rect.right - end_game_rect.left) / (float) 2) - 20;
        repeat_symbol.center.x = ((end_game_rect.right + end_game_rect.left) / (float) 2);
        repeat_symbol.center.y = end_game_rect.bottom - repeat_symbol.radius - 10;
        DrawRepeatSymbol(&wnd.image, &repeat_symbol, 10, &repeat_symbol_color);
    }
}

void HandlePaintEvent(HWND hwnd, HDC hdc)
{
    if(!hdc) { printf("Failed to retrieve handle to device context!\n"); }

}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

    wnd = CreateMSWindow(WindowProc, hInstance, "My Class");

    MSG msg;

    ShowWindow(wnd.hWnd, nCmdShow);

    InitializeGameState(&state);
    struct RGB chroma_key = {.r=255, .g=255, .b=255, .a=0};
    //struct Texture quad_tex = CreateTexture("rsc/Spr_Quadropus.bmp", &chroma_key);
    //Spr_SandWitch = CreateTexture("rsc/Spr_SandWitch.bmp", &chroma_key);
    //struct Texture Bone_Sand = CreateTexture("rsc/T_BoneSand.bmp", &chroma_key);
    //struct Texture venus_tex = CreateTexture("rsc/VENUS.bmp", &chroma_key);
    //struct Texture space_bkg = CreateTexture("rsc/space_background.bmp", &chroma_key);

    // Loop until WM_QUIT message is received (can quit program manually with PostQuitMessage(0);)
    // Once GetMessage receives WM_QUIT it doesn't even pass it to Window Procedure to handle it, it just returns 0
    // DispatchMessage is what passed the message to the window's Window Procedure, so it doesn't even have a chance
    // A message sent by the OS can skip the queue, this is called "Sending a message" (goes directly to WindProc)
    // Whereas the event where a message gets put in the queue is called "Posting a message"
    //while( GetMessage(&msg, NULL, 0, 0) ) // msg will contain message details, NULL = want messages for all windows
    while( state.running )
    {
        struct RGB bkgnd = {.r=0, .g=0, .b=0, .a=0};
        WritePixels32(&wnd.image, &bkgnd);
        if(!state.paused && !state.game_over) { UpdateGameState(&state); }
        DrawGameState();

    uint32_t player_loc_x_px = state.m_to_px_conversion_factor_x * state.player.entity.coords.x_meter;
    player_loc_x_px -= (state.player.entity.tex.rect.right - state.player.entity.tex.rect.left) / (float) 2.0;
    uint32_t player_loc_y_px = state.m_to_px_conversion_factor_y * state.player.entity.coords.y_meter;
    player_loc_y_px -= (state.player.entity.tex.rect.bottom - state.player.entity.tex.rect.top) / (float) 2.0;
    PlaceTexture(&state.player.entity.tex, &wnd.image, player_loc_x_px, player_loc_y_px, AlphaBinary);

        /*PlaceTexture(&quad_tex, &wnd.image, 50, 50, AlphaBinary);
        PlaceTexture(&Spr_SandWitch, &wnd.image, 150, 50, AlphaBinary);
        PlaceTexture(&Bone_Sand, &wnd.image, 100, 400, AlphaBinary);
        PlaceTexture(&venus_tex, &wnd.image, 50, 300, AlphaIgnore);*/
        //PlaceTexture(&space_bkg, &wnd.image, 50, 100, AlphaBinary);

        UpdateMSWindow(&wnd);

        // Peek message is used to be non-blocking, if a message is there, it passes it along, but otherwise it will just ignore it.
        if(PeekMessage(&msg, wnd.hWnd, 0, 0, PM_REMOVE)) // msg will contain message details, NULL = want messages for all windows.
        {
            TranslateMessage(&msg); // Translates things such as keystrokes into characters
            DispatchMessage(&msg); // Send the message to the Window Procedure for whatever window is associated with that message
        }
    }

    DestroyMSWindow(&wnd);

    //DestroyTexture(&quad_tex);
    /*DestroyTexture(&test_tex);
    DestroyTexture(&test_tex2);
    DestroyTexture(&test_tex3);*/

    DestroyGameState(&state);



    return 0;
}


