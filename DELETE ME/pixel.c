
#include "pixel.h"
#include "bitmap.h"
#include "stdlib.h"
#include "common.h"


static inline void CheckCoordinateBounds(uint32_t max_width, uint32_t max_height, uint32_t* x1, uint32_t* y1, uint32_t* x2, uint32_t* y2)
{
    if(x1) { if(*x1 < 0) *x1 = 0; if(*x1 >= max_width) *x1 = max_width - 1; }
    if(y1) { if(*y1 < 0) *y1 = 0; if(*y1 >= max_height) *y1 = max_height - 1; }
    if(x2) { if(*x2 < 0) *x2 = 0; if(*x2 >= max_width) *x2 = max_width - 1; }
    if(y2) { if(*y2 < 0) *y2 = 0; if(*y2 >= max_height) *y2 = max_height - 1; }
}

static inline uint8_t OutOfMemoryBounds( Bitmap* bitmap, uint32_t* pixel )
{
    uint32_t* upper_bounds = (uint32_t*) bitmap->pixels + ( bitmap->width * bitmap->height );
    if(pixel > upper_bounds)
        return 1;

    if(pixel < (uint32_t*) bitmap->pixels)
        return 1;
}


void ColorPixels(Bitmap* bitmap, RGB* rgb)
{
    uint32_t* pixel = (uint32_t*) bitmap->pixels; // First pixel

    for(uint32_t i = 0; i < bitmap->height; i++)
    {
        for(uint32_t j = 0; j < bitmap->width; j++)
        {
            *(pixel++) = rgb->a << 24 | rgb->r << 16 | rgb->g << 8 | rgb->b;
        }
    }
}

void ColorPixel(uint32_t* pixel, RGB* rgb)
{
    *(pixel) = rgb->a << 24 | rgb->r << 16 | rgb->g << 8 | rgb->b;
}

static inline void GetNextLinePoint(float m, int* x_px, int* y_px)
{
    m = m > 0 ? m : -m;

    int x = *x_px;
    int y = *y_px;

    float err = y - (m * x);

    if( m < 1 )
    {
        err -= m;
        x++;

        if(err < -0.5)
        {
            y++;
        }
    }
    else if(m >= 1)
    {
        err += 1;
        y++;

        if(err > 0.5)
        {
            x++;
        }
    }

    *x_px = x;
    *y_px = y;
}


static inline void GetNextQuarterCirclePoint(int* x_px, int* y_px, int r)
{
    int x = *x_px;
    int y = *y_px;

    float err = (r*r) - (x*x) - (y*y);

    if( x <= y )
    {
        err -= 2*x + 1;
        x++;

        if(err < -0.5)
        {
            y--;
        }
    }
    else if( x >= y )
    {
        err += 2*y - 1;
        y--;

        if(err > 0.5)
        {
            x++;
        }
    }

    *x_px = x;
    *y_px = y;
}

void DrawQuarterCircle(Bitmap* bitmap, Circle_px* circle, Quadrant quadrant, RGB* color)
{
    int x = 0, y = circle->radius;

    // Start on top center portion of circle -> (0, radius)
    int start_x = x, start_y = y + circle->radius;
    int y_loc, x_loc;
    int relative_pixel_loc;

    uint32_t* pixel;
    uint32_t* first_pixel = (uint32_t*) bitmap->pixels;

    while( y >= 0 )
    {
        switch(quadrant)
        {
            case TOP_RIGHT:
            {
                x_loc = circle->center.x + x;
                y_loc = circle->center.y - y;
            } break;

            case TOP_LEFT:
            {
                x_loc = circle->center.x - x;
                y_loc = circle->center.y - y;
            } break;

            case BOTTOM_LEFT:
            {
                x_loc = circle->center.x - x;
                y_loc = circle->center.y + y;

            } break;

            case BOTTOM_RIGHT:
            {
                x_loc = circle->center.x + x;
                y_loc = circle->center.y + y;
            } break;

            default:
            {
                printf("Invalid quadrant of %i\n", quadrant);
            } break;
        }

        // This is to cover a bug where circle was being drawn on opposite side of screen (overlapped to other end)
        if(y_loc < 0 || x_loc < 0)
            continue;

        relative_pixel_loc = (bitmap->width * y_loc) + x_loc;
        pixel = first_pixel + relative_pixel_loc;

        if( !OutOfMemoryBounds( bitmap, pixel ) )
        {
            ColorPixel(pixel, color);
        }

        GetNextQuarterCirclePoint(&x, &y, circle->radius);
    }
}

void DrawCircle(Bitmap* bitmap, Circle_px* circle, RGB* color)
{
    int x = 0, y = circle->radius;

    // Start on top center portion of circle -> (0, radius)
    int start_x = x, start_y = y + circle->radius;
    int y_loc, x_loc;
    int relative_pixel_loc;

    uint32_t* pixel;
    uint32_t* first_pixel = (uint32_t*) bitmap->pixels;

    while( y >= 0 )
    {
        // We draw only a quarter of a circle and mirror it to the other 3 quadrants
        for(int i = 0; i < 2; i++)
        {
            if(i)
            {
                y_loc = circle->center.y - y;
            }
            else
            {
                y_loc = circle->center.y + y;
            }

            for(int j = 0; j < 2; j++)
            {
                if(j)
                {
                    x_loc = circle->center.x + x;
                }
                else
                {
                    x_loc = circle->center.x - x;
                }

                // This is to cover a bug where circle was being drawn on opposite side of screen (overlapped to other end)
                if(y_loc < 0 || x_loc < 0)
                    continue;

                relative_pixel_loc = (bitmap->width * y_loc) + x_loc;
                pixel = first_pixel + relative_pixel_loc;

                if( !OutOfMemoryBounds( bitmap, pixel ) )
                {
                    ColorPixel(pixel, color);
                }
            }
        }

        GetNextQuarterCirclePoint(&x, &y, circle->radius);
    }
}

void DrawRepeatSymbol(Bitmap* bitmap, Circle_px* circle, uint32_t thickness, RGB* color)
{
    uint32_t stop_radius = circle->radius - thickness;
    for( int i = circle->radius; i > stop_radius; i-- )
    {
        circle->radius = i;
        DrawQuarterCircle(bitmap, circle, BOTTOM_LEFT, color);
        DrawQuarterCircle(bitmap, circle, BOTTOM_RIGHT, color);
        DrawQuarterCircle(bitmap, circle, TOP_LEFT, color);
    }

    Triangle_px arrow_head;
    arrow_head.base_line.p1.x = circle->center.x;
    arrow_head.base_line.p1.y = circle->center.y - circle->radius - (thickness * (3/(float)2));
    arrow_head.base_line.p2.x = arrow_head.base_line.p1.x;
    arrow_head.base_line.p2.y = circle->center.y - (circle->radius - (thickness / 2));

    arrow_head.tip_point.x = arrow_head.base_line.p1.x + (thickness * 2);
    arrow_head.tip_point.y = (arrow_head.base_line.p1.y + arrow_head.base_line.p2.y) / (float) 2;

    //DrawTriangle(bitmap, &arrow_head, color);
    FillTriangle(bitmap, &arrow_head, color);
}

void FillTriangle(Bitmap* bitmap, Triangle_px* triangle, RGB* color)
{
    Line_px line;
    line.p1.x = triangle->tip_point.x;
    line.p1.y = triangle->tip_point.y;

    int start_x = triangle->base_line.p1.x, start_y = triangle->base_line.p1.y;
    int end_x = triangle->base_line.p2.x, end_y = triangle->base_line.p2.y;

    if( start_x > end_x )
    {
        int temp = start_x;
        start_x = end_x;
        end_x = temp;

        temp = start_y;
        start_y = end_y;
        end_y = temp;
    }

    // Negate due to bitmap being top down and coordinate axes being bottom up
    float top = start_y - end_y;
    float bottom = end_x - start_x;
    float slope;
    if(bottom != 0)
    {
        slope = top / bottom;
    }
    
    int x = 0, y = 0;
    int x_loc = 0, y_loc = 0;

    // TODO: This is currently pretty ugly and can probably be made more clean
    //          I think the issue with mixing the two is within the "GetNextLinePoint" function -> since slop = 0
    //          I don't want to take the time to look more into this. But this is a great place to look when wanting to shorten the code
    if(bottom)
    {
        while( x_loc <= end_x )
        {
            if( slope > 0 )
            {
                y_loc = start_y - y;
            }
            else
            {
                y_loc = start_y + y;
            }

            x_loc = start_x + x;

            line.p2.x = x_loc;
            line.p2.y = y_loc;
            DrawLine(bitmap, &line, color);

            GetNextLinePoint(slope, &x, &y);
        }
    }
    else
    {
        line.p2.x = start_x;
        if(start_y > end_y)
        {
            int temp = start_y;
            start_y = end_y;
            end_y = temp;
        }
        y_loc = start_y;

        //while( (start_y < end_y && y_loc <= end_y) || (start_y > end_y && y_loc >= end_y) )
        while( y_loc <= end_y )
        {
            line.p2.y = y_loc++;
            DrawLine(bitmap, &line, color);
        }
    }
}

void DrawTriangle(Bitmap* bitmap, Triangle_px* triangle, RGB* color)
{
    Line_px line1;
    line1.p1.x = triangle->tip_point.x;
    line1.p1.y = triangle->tip_point.y;
    line1.p2.x = triangle->base_line.p1.x;
    line1.p2.y = triangle->base_line.p1.y;

    Line_px line2;
    line2.p1.x = triangle->tip_point.x;
    line2.p1.y = triangle->tip_point.y;
    line2.p2.x = triangle->base_line.p2.x;
    line2.p2.y = triangle->base_line.p2.y;

    DrawLine(bitmap, &line1, color);
    DrawLine(bitmap, &line2, color);
    DrawLine(bitmap, &triangle->base_line, color);
}

void DrawHorizontalLine(Bitmap* bitmap, uint32_t start_x, uint32_t start_y, uint32_t end_x, RGB* color)
{
    //CheckCoordinateBounds(bitmap->width, bitmap->height, &start_x, &start_y, &end_x, NULL);

    uint32_t* pixel = (uint32_t*) bitmap->pixels;
    pixel += (bitmap->width * start_y) + start_x;

    for(int x = start_x; x < end_x; x++)
    {
        if( OutOfMemoryBounds( bitmap, pixel ) )
            return;

        ColorPixel(pixel++, color);
    }
}

void DrawVerticalLine(Bitmap* bitmap, uint32_t start_x, uint32_t start_y, uint32_t end_y, RGB* color)
{
    //CheckCoordinateBounds(bitmap->width, bitmap->height, &start_x, &start_y, NULL, &end_y);

    uint32_t* pixel = (uint32_t*) bitmap->pixels;
    pixel += (bitmap->width * start_y) + start_x;

    for(int y = start_y; y < end_y; y++)
    {
        if( OutOfMemoryBounds( bitmap, pixel ) )
            return;

        ColorPixel(pixel, color);
        pixel += bitmap->width;
    }
}

void FillRectangle(Bitmap* bitmap, Rect_px* rect, RGB* color)
{
    uint32_t height = rect->bottom - rect->top;
    for(int y = 0; y < height; y++)
    {
        DrawHorizontalLine(bitmap, rect->left, y + rect->top, rect->right, color);
    }
}

void DrawRectangle(Bitmap* bitmap, Rect_px* rect, RGB* color)
{
    DrawHorizontalLine(bitmap, rect->left, rect->top, rect->right, color);
    DrawHorizontalLine(bitmap, rect->left, rect->bottom, rect->right, color);
    DrawVerticalLine(bitmap, rect->left, rect->top, rect->bottom, color);
    DrawVerticalLine(bitmap, rect->right, rect->top, rect->bottom, color);
}

void DrawLine(Bitmap* bitmap, Line_px* line_px, RGB* color)
{
    // Line is Vertical
    if( line_px->p1.x == line_px->p2.x )
    {
        DrawVerticalLine(bitmap, line_px->p1.x, line_px->p1.y, line_px->p2.y, color);
        return;
    }

    // Line is Horizontal
    if( line_px->p1.y == line_px->p2.y )
    {
        DrawHorizontalLine(bitmap, line_px->p1.x, line_px->p1.y, line_px->p2.x, color);
        return;
    }


    int start_x = line_px->p1.x, start_y = line_px->p1.y;
    int end_x = line_px->p2.x, end_y = line_px->p2.y;

    if( start_x > end_x )
    {
        int temp = start_x;
        start_x = end_x;
        end_x = temp;

        temp = start_y;
        start_y = end_y;
        end_y = temp;
    }

    uint32_t* pixel;

    // Negate due to bitmap being top down and coordinate axes being bottom up
    float slope = (start_y - end_y) / (float) (end_x - start_x);
    
    int x = 0, y = 0;
    int x_loc, y_loc;
    int relative_pixel_loc;

    while( x < (end_x - start_x) )
    {
        if( slope > 0 )
        {
            y_loc = start_y - y;
        }
        else
        {
            y_loc = start_y + y;
        }

        x_loc = start_x + x;

        //CheckCoordinateBounds(bitmap->width, bitmap->height, &x_loc, &y_loc, 0, 0);
    
        relative_pixel_loc = (bitmap->width * y_loc) + x_loc;

        pixel = ((uint32_t*) bitmap->pixels) + relative_pixel_loc;

        if( OutOfMemoryBounds( bitmap, pixel ) )
            return; 

        ColorPixel(pixel, color);

        GetNextLinePoint(slope, &x, &y);
    }



}



Texture CreateTexture(char* file_location)
{
    Texture texture;
    
    texture.file_location = file_location;

    texture.bmi = malloc(sizeof(BITMAPINFO));

    texture.image_data = LoadBitmapImage((const char*) texture.file_location, 
                                         &(texture.bmi));

    texture.width = texture.bmi->bmiHeader.biWidth;
    printf("texture.bmi_header.biwidth = %i\n", texture.width);
    texture.height = texture.bmi->bmiHeader.biHeight;
    printf("texture.bmi_header.biheight = %i\n", texture.height);

    texture.rect.left = 0;
    texture.rect.top = 0;
    texture.rect.right = 0;
    texture.rect.bottom = 0;

    return texture;
}

void DestroyTexture(Texture* texture)
{
    free(texture->image_data);
    free(texture->bmi);
}

void PlaceTexture(Bitmap* bitmap, Texture* texture, uint32_t x_pos, uint32_t y_pos)
{
    uint32_t* first_pixel = (uint32_t*) bitmap->pixels;
    uint32_t* pixel = first_pixel;

    uint32_t bitmap_row_size = bitmap->bytes_per_pixel * bitmap->width;
    //printf("Bitmap row size = %i\n", bitmap_row_size);

    texture->rect.left = x_pos;
    texture->rect.top = y_pos;
    texture->rect.right = texture->rect.left + texture->width;
    texture->rect.bottom = texture->rect.top + texture->height;

    pixel += ((texture->rect.top * bitmap->width) + texture->rect.left);
    //for(int i =0; i < texture->width; i++) { *(pixel) = 0; *(pixel++) = 255;}
    //pixel = first_pixel + ((texture->rect.top*bitmap->width) + texture->rect.left);
    // The offset to go from one row in image to next row in image relative to the bitmap
    uint32_t image_row_offset_px = (bitmap->width - texture->width);

    int image_index = 0;

    BMPMask bit_mask; 
    RGB rgb = {.a=0, .r=0, .g=0, .b=0};
    for(int image_row = 0; image_row < texture->height; image_row++)
    {
        for(int image_col = 0; image_col < texture->width; image_col++)
        {
            if(texture->bmi->bmiHeader.biClrUsed)
            {
                rgb.a = texture->bmi->bmiColors[texture->image_data[image_index]].rgbReserved;
                rgb.r = texture->bmi->bmiColors[texture->image_data[image_index]].rgbRed;
                rgb.g = texture->bmi->bmiColors[texture->image_data[image_index]].rgbGreen;
                rgb.b = texture->bmi->bmiColors[texture->image_data[image_index]].rgbBlue;
                image_index += 1;
            }
            else if(texture->bmi->bmiHeader.biCompression == 3)
            {
                // TODO: This is specific for 32 bits (and v4) TODO TODO: This should be read from the header!!! If true biCompression = 3 -> Would be in general then (not just 32 bit or v4)
                bit_mask.amask = 0xFF000000;
                bit_mask.rmask = 0x00FF0000;
                bit_mask.gmask = 0x0000FF00;
                bit_mask.bmask = 0x000000FF;

                bit_mask.ashift = __builtin_ffs(bit_mask.amask) - 1;
                bit_mask.rshift = __builtin_ffs(bit_mask.rmask) - 1;
                bit_mask.gshift = __builtin_ffs(bit_mask.gmask) - 1;
                bit_mask.bshift = __builtin_ffs(bit_mask.bmask) - 1;

                bit_mask.amax = bit_mask.amask >> bit_mask.ashift;
                bit_mask.rmax = bit_mask.rmask >> bit_mask.rshift;
                bit_mask.gmax = bit_mask.gmask >> bit_mask.gshift;
                bit_mask.bmax = bit_mask.bmask >> bit_mask.bshift;

                uint32_t pixel = 0;
                pixel |= texture->image_data[image_index++]; pixel <<= 8;
                pixel |= texture->image_data[image_index++]; pixel <<= 8;
                pixel |= texture->image_data[image_index++]; pixel <<= 8;
                pixel |= texture->image_data[image_index++];

                uint32_t araw = (pixel & bit_mask.amask) >> bit_mask.ashift;
                uint32_t rraw = (pixel & bit_mask.rmask) >> bit_mask.rshift;
                uint32_t graw = (pixel & bit_mask.gmask) >> bit_mask.gshift;
                uint32_t braw = (pixel & bit_mask.bmask) >> bit_mask.bshift;


                /*rgb.a = (pixel && bit_mask.amask) >> 24;
                rgb.r = (pixel && bit_mask.rmask) >> 16;
                rgb.g = (pixel && bit_mask.gmask) >> 8;
                rgb.b = (pixel && bit_mask.bmask);*/
                /*rgb.a = pixel >> 24; //&& bit_mask.amask; // TODO: Alpha channel needs special care? -> On rendering level
                rgb.r = (pixel << 8) >> 24;// && bit_mask.rmask;
                rgb.g = (pixel << 16) >> 24;// && bit_mask.gmask;
                rgb.b = (pixel << 24) >> 24;// && bit_mask.bmask;*/

                rgb.a = (araw * 0xFF) / bit_mask.amax;
                rgb.r = (rraw * 0xFF) / bit_mask.rmax;
                rgb.g = (graw * 0xFF) / bit_mask.gmax;
                rgb.b = (braw * 0xFF) / bit_mask.bmax;


            }
            else
            {
                rgb.r = texture->image_data[image_index++];
                rgb.g = texture->image_data[image_index++];
                rgb.b = texture->image_data[image_index++];
            }

            if(rgb.a)
            {
                /*rgb.r = rgb.r + 
                rgb.g = 
                rgb.b = */
            }
            else
            {
                *(pixel++) = rgb.a << 24 | rgb.r << 16 | rgb.g << 8 | rgb.b;
            }
        }
        pixel += image_row_offset_px;
    }
}









