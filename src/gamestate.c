
#include "gamestate.h"
#include "common.h"
#include "xorshiftstar.h"
#include <stdlib.h>
#include <stdio.h>

#define KB 1024
#define MB (1024 * KB)
#define GB (1024 * MB)

// TODO: My collision detection is missing literal edge cases
// TODO: Maybe try some sort of vectorized form for collision detection?

// Handle the case of breaching the acceleration bounds
static void ValidateAcceleration(Physics* physics)
{
    if(physics->accel_x > MAX_ACCELERATION)
    {
        physics->accel_x = MAX_ACCELERATION;
    }
    else if(physics->accel_x < -MAX_ACCELERATION)
    {
        physics->accel_x = -MAX_ACCELERATION;
    }

    if(physics->accel_y > MAX_ACCELERATION)
    {
        physics->accel_y = MAX_ACCELERATION;
    }
    else if(physics->accel_y < -MAX_ACCELERATION)
    {
        physics->accel_y = -MAX_ACCELERATION;
    }

}

// Handle the case of breaching the acceleration bounds
static void ValidateVelocity(Physics* physics)
{
    if(physics->vel_x > MAX_VELOCITY)
    {
        physics->vel_x = MAX_VELOCITY;
    }
    else if(physics->vel_x < -MAX_VELOCITY)
    {
        physics->vel_x = -MAX_VELOCITY;
    }

    if(physics->vel_y > MAX_VELOCITY)
    {
        physics->vel_y = MAX_VELOCITY;
    }
    else if(physics->vel_y < -MAX_VELOCITY)
    {
        physics->vel_y = -MAX_VELOCITY;
    }

}

static void UpdateVelocity(Physics* physics)
{
    physics->vel_x = physics->vel_x + (physics->accel_x * (1 / (float) FRAMES_PER_SECOND));
    physics->vel_y = physics->vel_y + (physics->accel_y * (1 / (float) FRAMES_PER_SECOND));

    ValidateVelocity(physics);
}

static void ValidatePosition(Entity* entity)
{
    // TODO: Some impulse calculation thingy to bounce entity off, and use angle magic to make realistic -> Need mass
    //if(entity->coords.x_meter - (entity->size.width / 2) < 0)
    if(entity->coords.x_meter - (entity->size.width / 2) < 0)
    {
        entity->coords.x_meter = entity->size.width;
        //entity->physics.accel_x = -entity->physics.accel_x;
        entity->physics.vel_x = -entity->physics.vel_x;
    }
    else if(entity->coords.x_meter + (entity->size.width / 2) > SCREEN_WIDTH_METERS)
    {
        entity->coords.x_meter = SCREEN_WIDTH_METERS - (entity->size.width / 2);
        //entity->physics.accel_x = -entity->physics.accel_x;
        entity->physics.vel_x = -entity->physics.vel_x;
    }

    if(entity->coords.y_meter - (entity->size.height / 2) < 0)
    {
        entity->coords.y_meter = entity->size.height;
        //entity->physics.accel_y = -entity->physics.accel_y;
        entity->physics.vel_y = -entity->physics.vel_y;
    }
    else if(entity->coords.y_meter + (entity->size.height / 2) > SCREEN_HEIGHT_METERS)
    {
        entity->coords.y_meter = SCREEN_HEIGHT_METERS - (entity->size.height / 2);
        //entity->physics.accel_y = -entity->physics.accel_y;
        entity->physics.vel_y = -entity->physics.vel_y;
    }
}

// Ensure the entity is within the bounds of the map and if not then correct it
static void ValidateEntityWithinBounds(Entity* entity)
{
    Rect entity_rect = GetEntityRectangle(entity);

    // NOTE: Point for entity represents the entity's center
    // NOTE: The entity's center is guaranteed to be within the map's boundaries,
    //          so adding or subtracting half the entity's width to the x meter position would put the entity within the x bounds
    //          A similar statement can be said for the other bounds
    if(entity_rect.x_meter < 0)
    {
        entity->coords.x_meter += -entity_rect.x_meter;
        entity->physics.accel_x = -entity->physics.accel_x;
        entity->physics.vel_x = -entity->physics.vel_x;
    }
    else if(entity_rect.x_meter > SCREEN_WIDTH_METERS)
    {
        entity->coords.x_meter -= ((entity->coords.x_meter + entity_rect.width) - SCREEN_WIDTH_METERS);
        entity->physics.accel_x = -entity->physics.accel_x;
        entity->physics.vel_x = -entity->physics.vel_x;
    }

    //printf("BEFORE entity y coord = %i\n", entity->coords.y_meter);
    if(entity_rect.y_meter < 0)
    {
        entity->coords.y_meter += -entity_rect.y_meter;
        entity->physics.accel_y = -entity->physics.accel_y;
        entity->physics.vel_y = -entity->physics.vel_y;
    }
    else if(entity_rect.y_meter > SCREEN_HEIGHT_METERS)
    {
        entity->coords.y_meter -= ((entity->coords.y_meter + entity_rect.height) - SCREEN_HEIGHT_METERS);
        entity->physics.accel_y = -entity->physics.accel_y;
        entity->physics.vel_y = -entity->physics.vel_y;
    }
    //printf("AFTER entity y coord = %i\n", entity->coords.y_meter);
}

static void MoveEntity(Entity* entity)
{
    // The update velocity function already handles coverting m/s to velocity over a frame
    //printf("Move entity x_meter by %f\nMove entity y_meter by\n", entity->physics.vel_x, entity->physics.vel_y);
    //entity->coords.x_meter += entity->physics.vel_x;
    //entity->coords.y_meter += entity->physics.vel_y;

    float time_per_frame = 1 / (float) FRAMES_PER_SECOND;
    float time_squared = time_per_frame * time_per_frame;

    entity->coords.x_meter = entity->coords.x_meter + (entity->physics.vel_x * time_per_frame) + ( (entity->physics.accel_x * time_squared) / 2);
    entity->coords.y_meter = entity->coords.y_meter + (entity->physics.vel_y * time_per_frame) + ( (entity->physics.accel_y * time_squared) / 2);

    ValidateEntityWithinBounds(entity);
}

static void HandlePlayerMovement(Player* player)
{
    if(player->input.keydown[KeyUp])
    {
        player->entity.physics.accel_y = -CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_y++;
    }
    if(player->input.keydown[KeyDown])
    {
        player->entity.physics.accel_y = CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_y--;
    }
    if(player->input.keydown[KeyRight])
    {
        player->entity.physics.accel_x = CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_x--;
    }
    if(player->input.keydown[KeyLeft])
    {
        player->entity.physics.accel_x = -CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_x++;
    }

    if(!player->input.keydown[KeyUp] && !player->input.keydown[KeyDown])
    {
        player->entity.physics.accel_y = 0;
    }
    if(!player->input.keydown[KeyRight] && !player->input.keydown[KeyLeft])
    {
        player->entity.physics.accel_x = 0;
    }

    //ValidateAcceleration( &(player->entity.physics) );
    UpdateVelocity( &(player->entity.physics) );
    MoveEntity( &(player->entity) );
}

// Determine if 2 lines have collided
// Steps:
//     1. Standardize start and end points of line
//     2. Determine if lines have horizontal or vertical properties
//          2a. Handle collision detection if both lines have either horizontal or vertical properties
//     3. TODO Handle collision detection for the general case (lines don't have horizontal or vertical properties)
static inline uint8_t LineHasCollidedWithLine(struct Line* line1, struct Line* line2)
{
    // Step 1: Standardize start and end points TODO: Is this efficient (No)? Is there a better way?
    float start1_x = line1->p1.x_meter;
    float start1_y = line1->p1.y_meter;
    float end1_x = line1->p2.x_meter;
    float end1_y = line1->p2.y_meter;
    if( start1_x > end1_x || (start1_x == end1_x && start1_y > end1_y) || (start1_y == end1_y && start1_x > end1_x) )
    {
       start1_x = line1->p2.x_meter;
       start1_y = line1->p2.y_meter;
       end1_x = line1->p1.x_meter;
       end1_y = line1->p1.y_meter;
    }

    float start2_x = line2->p1.x_meter;
    float start2_y = line2->p1.y_meter;
    float end2_x = line2->p2.x_meter;
    float end2_y = line2->p2.y_meter;
    if( start2_x > end2_x  || (start2_x == end2_x && start2_y > end2_y) || (start2_y == end2_y && start2_x > end2_x) )
    {
       start2_x = line2->p2.x_meter;
       start2_y = line2->p2.y_meter;
       end2_x = line2->p1.x_meter;
       end2_y = line2->p1.y_meter;
    }

    // Step 2: Determine if lines have horizontal or vertical properties, handle collision detection if so
    // Both lines horizontal
    if( start1_y == end1_y && start2_y == end2_y )
    {
        //printf("hori\n");
        if( (start2_x <= end1_x && end1_x <= end2_x) && (start1_y == start2_y) ) return 1;
        else if( (start2_x <= start1_x && start1_x <= end2_x) && (start1_y == start2_y) ) return 1;
        else return 0;
    }
    // Both lines vertical
    else if( start1_x == end1_x && start2_x == end2_x )
    {
        //printf("verti\n");
        if( (start1_y <= start2_y && start2_y <= end1_y) && (start1_x == start2_x) ) return 1;
        else if( (start2_y <= start1_y && start1_y <= end2_y) && (start1_x == start2_x) ) return 1;
        else return 0;
    }
    // First line vertical, second line horizontal
    else if( start1_x == end1_x && start2_y == end2_y )
    {
        //printf("1 verti, 2 hori\n");
        if( (start1_y <= start2_y && start2_y <= end1_y) && (start2_x <= start1_x && start1_x <= end2_x) ) return 1;
        else return 0;
    }
    // First line horizontal, second line vertical
    else if( start1_y == end1_y && start2_x == end2_x )
    {
        //printf("1 hori, 2 verti\n");
        if( (start2_y <= start1_y && start1_y <= end2_y) && (start1_x <= start2_x && start2_x <= end1_x) ) return 1;
        else return 0;
    }

    return 0;

    // TODO Step 3: Handle collision detection for general case
}

// TODO: Ensure this is correct... Also find a better way?
// TODO: Does this cover the case of rect1 being larger than rect2 so that no corner of rect1 touches rect2 but still a collision?
static uint8_t RectHasCollidedWithRect(Rect* rect1, Rect* rect2, Point* rect1_overlap_point_out)
{
    // Steps:
    //     1. Grab all 4 lines for each of the rectangles
    //     2. See if any of the 4 lines have collided with another -> TODO: Bad... Inefficient
    
    struct Line left1 = {.p1.x_meter = rect1->x_meter, .p1.y_meter = rect1->y_meter,
                         .p2.x_meter = rect1->x_meter, .p2.y_meter = rect1->y_meter + rect1->height};
    struct Line right1 = {.p1.x_meter = rect1->x_meter + rect1->width, .p1.y_meter = rect1->y_meter,
                          .p2.x_meter = rect1->x_meter + rect1->width, .p2.y_meter = rect1->y_meter + rect1->height};
    struct Line top1 = {.p1.x_meter = rect1->x_meter, .p1.y_meter = rect1->y_meter,
                        .p2.x_meter = rect1->x_meter + rect1->width, .p2.y_meter = rect1->y_meter};
    struct Line bottom1 = {.p1.x_meter = rect1->x_meter, .p1.y_meter = rect1->y_meter + rect1->height,
                           .p2.x_meter = rect1->x_meter + rect1->width, .p2.y_meter = rect1->y_meter + rect1->height};

    struct Line rect1_lines[4] = {left1, right1, top1, bottom1};

    struct Line left2 = {.p1.x_meter = rect2->x_meter, .p1.y_meter = rect2->y_meter,
                         .p2.x_meter = rect2->x_meter, .p2.y_meter = rect2->y_meter + rect2->height};
    struct Line right2 = {.p1.x_meter = rect2->x_meter + rect2->width, .p1.y_meter = rect2->y_meter,
                          .p2.x_meter = rect2->x_meter + rect2->width, .p2.y_meter = rect2->y_meter + rect2->height};
    struct Line top2 = {.p1.x_meter = rect2->x_meter, .p1.y_meter = rect2->y_meter,
                        .p2.x_meter = rect2->x_meter + rect2->width, .p2.y_meter = rect2->y_meter};
    struct Line bottom2 = {.p1.x_meter = rect2->x_meter, .p1.y_meter = rect2->y_meter + rect2->height,
                           .p2.x_meter = rect2->x_meter + rect2->width, .p2.y_meter = rect2->y_meter + rect2->height};

    struct Line rect2_lines[4] = {left2, right2, top2, bottom2};

    for(uint8_t i = 0; i < 4; i++)
    {
        for(uint8_t j = 0; j < 4; j++)
        {
            if( LineHasCollidedWithLine(&rect1_lines[i], &rect2_lines[j]) )
            {
                // TODO: Perhaps a bitfield can be utilized here. Also, there seems to be some nice properties (0+1+2+3=6 always)
                // Top left collided into bottom right
                if( (i == 0 || i == 2) && (j == 1 || j == 3) )
                {
                    if(rect1_overlap_point_out)
                    {
                        rect1_overlap_point_out->x_meter = rect1->x_meter;
                        rect1_overlap_point_out->y_meter = rect1->y_meter;
                    }
                    
                    // TODO: All these corner returns are wrong, this catches below cases
                    //          Likely better to do side collision detection instead
                    return TOP_LEFT_INTO_BOTTOM_RIGHT;
                }
                // Top right collided into bottom left
                else if( (i == 1 || i == 2) && (j = 0 || j == 3) )
                {
                    if(rect1_overlap_point_out)
                    {
                        rect1_overlap_point_out->x_meter = rect1->x_meter + rect1->width;
                        rect1_overlap_point_out->y_meter = rect1->y_meter;
                    }
                    
                    return TOP_RIGHT_INTO_BOTTOM_LEFT;
                }
                // Bottom left collided into top right
                else if( (i == 0 || i == 3) && (j == 1 || j == 2) )
                {
                    if(rect1_overlap_point_out)
                    {
                        rect1_overlap_point_out->x_meter = rect1->x_meter;
                        rect1_overlap_point_out->y_meter = rect1->y_meter + rect1->height;
                    }
                    
                    return BOTTOM_LEFT_INTO_TOP_RIGHT;
                }
                // Bottom right collided into top left
                else if( (i == 1 || i == 3) && (j == 0 || j == 2) )
                {
                    if(rect1_overlap_point_out)
                    {
                        rect1_overlap_point_out->x_meter = rect1->x_meter + rect1->width;
                        rect1_overlap_point_out->y_meter = rect1->y_meter + rect1->height;
                    }
                    
                    return BOTTOM_RIGHT_INTO_TOP_LEFT;
                }
                else
                {
                    printf("Collision did not occur in a corner\n");
                }
            }
        }
    }
    return 0;

    /* This is the semi-bad, but more efficient than what I'm doing now, if corner within rect test
     * TODO: Perhaps move this to a "RectCornerWithinRect" function, since it is more efficient and good enough for many cases
    // Collision of top left of rect 1 into bottom right of rect 2
    if( (rect1->x_meter >= rect2->x_meter &&
         rect1->x_meter <= rect2->x_meter + rect2->width) &&
        (rect1->y_meter >= rect2->y_meter &&
         rect1->y_meter <= rect2->y_meter + rect2->height) )
    {
        if(rect1_overlap_point_out)
        {
            rect1_overlap_point_out->x_meter = rect1->x_meter;
            rect1_overlap_point_out->y_meter = rect1->y_meter;
        }
        return TOP_LEFT_INTO_BOTTOM_RIGHT;
    }
    // Collision of top right of rect 1 into bottom left of rect2
    else if( (rect1->x_meter + rect1->width >= rect2->x_meter &&
              rect1->x_meter + rect1->width <= rect2->x_meter + rect2->width) &&
             (rect1->y_meter >= rect2->y_meter &&
              rect1->y_meter <= rect2->y_meter + rect2->width) )
    {
        if(rect1_overlap_point_out)
        {
            rect1_overlap_point_out->x_meter = rect1->x_meter + rect1->width;
            rect1_overlap_point_out->y_meter = rect1->y_meter;
        }
        return TOP_RIGHT_INTO_BOTTOM_LEFT;
    }
    // Collision of bottom left of rect 1 into top right of rect2
    else if( (rect1->x_meter >= rect2->x_meter && 
              rect1->x_meter <= rect2->x_meter + rect2->width) &&
             (rect1->y_meter + rect1->height >= rect2->y_meter &&
              rect1->y_meter + rect1->height <= rect2->y_meter + rect2->height) )
    {
        if(rect1_overlap_point_out)
        {
            rect1_overlap_point_out->x_meter = rect1->x_meter;
            rect1_overlap_point_out->y_meter = rect1->y_meter + rect1->height;
        }
        return BOTTOM_LEFT_INTO_TOP_RIGHT;
    }
    // Collision of bottom right of rect 1 into top left of rect 2
    else if( (rect1->x_meter + rect1->width >= rect2->x_meter && 
         rect1->x_meter + rect1->width <= rect2->x_meter + rect2->width) &&
        (rect1->y_meter + rect1->height >= rect2->y_meter &&
         rect1->y_meter + rect1->height <= rect2->y_meter + rect2->height) )
    {
        if(rect1_overlap_point_out)
        {
            rect1_overlap_point_out->x_meter = rect1->x_meter + rect1->width;
            rect1_overlap_point_out->y_meter = rect1->y_meter + rect1->height;
        }
        return BOTTOM_RIGHT_INTO_TOP_LEFT;
    }*/

    return 0;
}

static Line CreateLine(Point* p1, Point* p2)
{
    Line line;

    line.p1.x_meter = p1->x_meter;
    line.p1.y_meter = p1->y_meter;

    line.p2.x_meter = p2->x_meter;
    line.p2.y_meter = p2->y_meter;

    line.slope = (line.p2.y_meter - line.p1.y_meter) / (line.p2.x_meter - line.p1.x_meter);

    line.y_intercept = line.p1.y_meter - (line.slope * line.p1.x_meter);

    return line;
}

static Point PointOnCircleGivenLine(Circle* circle, Line* line, uint8_t y_coord_pos_flag)
{
    Point coords;

    float b_k_difference = line->y_intercept - circle->center.y_meter;

    float a = Exponentiate(line->slope, 2) + 1;
    float b = (2 * line->slope * b_k_difference) - (2 * circle->center.x_meter);
    float c = Exponentiate(b_k_difference, 2) + Exponentiate(circle->center.x_meter, 2) - Exponentiate(circle->radius, 2);

    float sqrt_portion = NthRoot( 2, Exponentiate(b, 2) - (4 * a * c) );
    
    float root_1 = ( -b + sqrt_portion) / (2 * a);
    float root_2 = ( -b - sqrt_portion) / (2 * a);

    float y_coord_given_root_1 = (line->slope * root_1) + line->y_intercept;
    float y_coord_given_root_2 = (line->slope * root_2) + line->y_intercept;
    
    // NOTE: A ternary operator would just be ugly, but there must certaintly still be a better way...
    // If we want the positive y coordinate
    if(y_coord_pos_flag)
    {
        if(y_coord_given_root_1 > 0)
        {
            coords.x_meter = root_1;
            coords.y_meter = y_coord_given_root_1;
        }
        else
        {
            coords.x_meter = root_2;
            coords.y_meter = y_coord_given_root_2;
        }
    }
    else
    {
        if(y_coord_given_root_1 < 0)
        {
            coords.x_meter = root_1;
            coords.y_meter = y_coord_given_root_1;
        }
        else
        {
            coords.x_meter = root_2;
            coords.y_meter = y_coord_given_root_2;
        }
    }
    
    return coords;
}

static enum Quadrant GetQuadrantAndNearestRectCornerToCircle(Rect* rect, Circle* circle, Point* coords_out)
{
    enum Quadrant quadrant;

    // NOTE: Coordinate comparison is relative to the circle's center (i.e. x/y plane has origin at circle center)
    // Rectangle is upper right - relative to circle center
    if( rect->x_meter >= circle->center.x_meter && rect->y_meter >= circle->center.y_meter)
    {
        //y_coord_pos_flag = 1;
        quadrant = TOP_RIGHT;

        coords_out->x_meter = rect->x_meter;
        coords_out->y_meter = rect->y_meter + rect->height;
    }
    // Rectangle is upper left - relative to circle center
    else if( rect->x_meter <= circle->center.x_meter && rect->y_meter >= circle->center.y_meter)
    {
        //y_coord_pos_flag = 1;
        quadrant = TOP_LEFT;

        coords_out->x_meter = rect->x_meter + rect->width;
        coords_out->y_meter = rect->y_meter + rect->height;
    }
    // Rectangle is lower right - relative to circle center
    else if( rect->x_meter >= circle->center.x_meter && rect->y_meter <= circle->center.y_meter)
    {
        //y_coord_pos_flag = 0;
        quadrant = BOTTOM_RIGHT;

        coords_out->x_meter = rect->x_meter;
        coords_out->y_meter = rect->y_meter;
    }
    // Top right of rectangle collided with bottom left of circle
    // Rectangle is lower left - relative to circle center
    else if( rect->x_meter <= circle->center.x_meter && rect->y_meter <= circle->center.y_meter)
    {
        //y_coord_pos_flag = 0;
        quadrant = BOTTOM_LEFT;

        coords_out->x_meter = rect->x_meter + rect->width;
        coords_out->y_meter = rect->y_meter;
    }
    else
    {
        quadrant = INVALID;
    }
    
    return quadrant;
}

static float CalculateDistanceBetweenPoints(Point* p1, Point* p2)
{
    return NthRoot(2, Exponentiate(p2->x_meter - p1->x_meter, 2) + Exponentiate(p2->y_meter - p1->y_meter, 2) );
}

// See if the rectangle has a corner within the circle, do this by drawing a line from the cirlce's center to that nearest corner
//      if this distance is less than the radius, then the rect corner is within the circle.
static uint8_t RectCornerWithinCircle(Rect* rect, Circle* circle)
{
    Point p1;
    Point p2 = {.x_meter = circle->center.x_meter, .y_meter = circle->center.y_meter};
    enum Quadrant quadrant = GetQuadrantAndNearestRectCornerToCircle(rect, circle, &p1);

    float dist_from_circle_center_to_rect_corner = CalculateDistanceBetweenPoints(&p1, &p2);

    if( dist_from_circle_center_to_rect_corner < circle->radius )
    {
        return 1;
    }
    
    return 0;
}

// TODO: Ensure this is correct... Also, find a better way?
static uint8_t RectHasCollidedWithCircle(Rect* rect, Circle* circle)
{
    Point p1;
    Point p2 = {.x_meter = circle->center.x_meter, .y_meter = circle->center.y_meter};

    //uint8_t y_coord_pos_flag;
    enum Quadrant quadrant = GetQuadrantAndNearestRectCornerToCircle(rect, circle, &p1);

    Line line_through_rect_and_circle_center = CreateLine(&p1, &p2);
    Point point_on_circle_and_line = PointOnCircleGivenLine(circle, &line_through_rect_and_circle_center, quadrant > 0);

    // NOTE: Because the quadrant is relative to the circle's center and because 
    //          we know what the rectangle's corresponding corner (and what that corner's quadrant is), 
    //          we only need to test the point on the cirlce and the point on the rectangle's corner
    // NOTE: Point here are relative to an origin in the top left corner of screen (like normal)
    //          But quadrant is relative to circle's center
    switch(quadrant)
    {
        case TOP_RIGHT:
        {
            if( (rect->x_meter <= point_on_circle_and_line.x_meter &&
                 rect->y_meter >= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
        case TOP_LEFT:
        {
            if( (rect->x_meter >= point_on_circle_and_line.x_meter &&
                 rect->y_meter >= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
        case BOTTOM_LEFT:
        {
            if( (rect->x_meter >= point_on_circle_and_line.x_meter &&
                 rect->y_meter <= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
        case BOTTOM_RIGHT:
        {
            if( (rect->x_meter <= point_on_circle_and_line.x_meter &&
                 rect->y_meter <= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
    }

    return 0;
}

    
static inline void ValidateNoEntityOverlap(Entity* entity, Entity* other_entities, uint32_t num_entities)
{
    Rect entity_rect = GetEntityRectangle( entity );
    // Ensure no enemies overlap
    for(int j = 0; j < num_entities; j++)
    {
        Rect other_entity_rect = GetEntityRectangle( &other_entities[j]);
        Point rect1_overlap_point;
        float x_diff = 0, y_diff = 0;
        switch(RectHasCollidedWithRect(&entity_rect, &other_entity_rect, &rect1_overlap_point))
        {
            // TODO: Maybe this is the same as "Bottom_right into top left"? -> May be able to simplify
            // TODO: There is a lot of repeated code here with the adjustment conditionals -> Find way to simplify
            // We are wanting to find the difference in meters betwen the collision corner of rect1 and rect2's edge
            case TOP_LEFT_INTO_BOTTOM_RIGHT:
            {
                x_diff = (other_entity_rect.x_meter + other_entity_rect.width) - rect1_overlap_point.x_meter;
                y_diff = (other_entity_rect.y_meter + other_entity_rect.height) - rect1_overlap_point.y_meter;

                // Attempt to move rect1 to the right the overlapped amount
                if( (entity_rect.x_meter + entity_rect.width) + x_diff < SCREEN_WIDTH_METERS )
                {
                    entity->coords.x_meter += x_diff;
                    entity->physics.accel_x = CONSTANT_ACCEL;
                    other_entities[j].physics.accel_x = -entity->physics.accel_x;
                }
                // Else, move rect2 to the left the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter -= x_diff;
                    other_entities[j].physics.accel_x = -CONSTANT_ACCEL;
                    entity->physics.accel_x = CONSTANT_ACCEL;
                }

                // TODO: This shouldn't be needed, as long as we can move the rect in some direction out of rectangle
                // Attempt to move rect1 down the overlapped amount
                if( (entity_rect.y_meter + entity_rect.height) + y_diff < SCREEN_HEIGHT_METERS )
                {
                    entity->coords.y_meter += y_diff;
                    entity->physics.accel_y = CONSTANT_ACCEL;
                    other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                }
                // Else, move rect2 up the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter -= y_diff;
                    other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                    entity->physics.accel_y = CONSTANT_ACCEL;
                }
            } break;

            case TOP_RIGHT_INTO_BOTTOM_LEFT:
            {
                x_diff = rect1_overlap_point.x_meter - (other_entity_rect.x_meter);
                y_diff = (other_entity_rect.y_meter + other_entity_rect.height) - rect1_overlap_point.y_meter;

                // Attempt to move rect1 to the left the overlapped amount
                if( (entity_rect.x_meter) - x_diff > 0 )
                {
                    entity->coords.x_meter -= x_diff;
                    entity->physics.accel_x = -CONSTANT_ACCEL;
                    other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                }
                // Else, move rect2 to the right the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter += x_diff;
                    other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                    entity->physics.accel_x = -CONSTANT_ACCEL;
                }

                // Attempt to move rect1 down the overlapped amount
                if( (entity_rect.y_meter + entity_rect.height) + y_diff < SCREEN_HEIGHT_METERS )
                {
                    entity->coords.y_meter += y_diff;
                    entity->physics.accel_y = CONSTANT_ACCEL;
                    other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                }
                // Else, move rect2 up the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter -= y_diff;
                    other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                    entity->physics.accel_y = CONSTANT_ACCEL;
                }
            } break;

            case BOTTOM_LEFT_INTO_TOP_RIGHT:
            {
                x_diff = (other_entity_rect.x_meter + other_entity_rect.width) - rect1_overlap_point.x_meter;
                y_diff = rect1_overlap_point.y_meter - (other_entity_rect.y_meter);

                // Attempt to move rect1 to the right the overlapped amount
                if( (entity_rect.x_meter + entity_rect.width) + x_diff < SCREEN_WIDTH_METERS )
                {
                    entity->coords.x_meter += x_diff;
                    entity->physics.accel_x = CONSTANT_ACCEL;
                    other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                }
                // Else, move rect2 to the left the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter -= x_diff;
                    other_entities[j].physics.accel_x = -CONSTANT_ACCEL;
                    entity->physics.accel_x = CONSTANT_ACCEL;
                }

                // Attempt to move rect1 up the overlapped amount
                if( (entity_rect.y_meter) - y_diff > 0 )
                {
                    entity->coords.y_meter -= y_diff;
                    entity->physics.accel_y = -CONSTANT_ACCEL;
                    other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                }
                // Else, move rect2 down the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter += y_diff;
                    other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                    entity->physics.accel_y = -CONSTANT_ACCEL;
                }
            } break;

            case BOTTOM_RIGHT_INTO_TOP_LEFT:
            {
                x_diff = rect1_overlap_point.x_meter - (other_entity_rect.x_meter);
                y_diff = rect1_overlap_point.y_meter - (other_entity_rect.y_meter);

                // Attempt to move rect1 to the left the overlapped amount
                if( (entity_rect.x_meter) - x_diff > 0 )
                {
                    entity->coords.x_meter -= x_diff;
                    entity->physics.accel_x = -CONSTANT_ACCEL;
                    other_entities[j].physics.accel_x = -CONSTANT_ACCEL;
                }
                // Else, move rect2 to the right the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter += x_diff;
                    other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                    entity->physics.accel_x = -CONSTANT_ACCEL;
                }

                // Attempt to move rect1 up the overlapped amount
                if( (entity_rect.y_meter) - y_diff > 0 )
                {
                    entity->coords.y_meter -= y_diff;
                    entity->physics.accel_y = -CONSTANT_ACCEL;
                    other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                }
                // Else, move rect2 down the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter += y_diff;
                    other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                    entity->physics.accel_y = -CONSTANT_ACCEL;
                }
            } break;
        }
    }
}


static void HandleCollisionDetection(GameState* state)
{
    Rect player_rect = GetEntityRectangle(&(state->player.entity));

    for(int i = 0; i < state->num_enemies; i++)
    {
        Rect enemy_rect = GetEntityRectangle(&(state->enemies[i].entity));

        if(RectHasCollidedWithRect(&player_rect, &enemy_rect, 0) || RectHasCollidedWithRect(&enemy_rect, &player_rect, 0))
        {
            state->game_over = 1;
        }
    }
}

Circle GetProximityCircle(Player* player, Difficulty difficulty)
{
    Circle result;

    float proximity_limit;
    switch(difficulty)
    {
        case Demo:
        {
            proximity_limit = PROXIMITY_DEMO;
        } break;

        case Easy:
        {
            proximity_limit = PROXIMITY_EASY;
        } break;

        case Medium:
        {
            proximity_limit = PROXIMITY_MEDIUM;
        } break;

        case Hard:
        {
            proximity_limit = PROXIMITY_HARD;
        } break;

        case Impossible:
        {
            proximity_limit = PROXIMITY_IMPOSSIBLE;
        } break;

        default:
        {
            printf("WARNING: Difficulty was not appropriately set, using difficulty of Demo!\n");
            proximity_limit = PROXIMITY_DEMO;
        } break;
    }

    result.center.x_meter = player->entity.coords.x_meter;
    result.center.y_meter = player->entity.coords.y_meter;
    result.radius = proximity_limit;

    return result;
}

static void CreateEnemy(Difficulty difficulty, Enemy** enemies, uint32_t* num_enemies, Player* player)
{
    *enemies = realloc(*enemies, sizeof(Enemy) * ++(*num_enemies));
    ValidateObject(*enemies, "Realloc of Enemies");

    Circle proximity_circle = GetProximityCircle(player, difficulty);

    Rect player_rect = GetEntityRectangle( &(player->entity) );
    int enemy_index = *(num_enemies) - 1;

    // Using the two lines to adjust enemy width to essentially use the uniform float function as a range function
    (*enemies)[enemy_index].entity.size.width = xorshift64star_float(MAX_ENTITY_WIDTH - MIN_ENTITY_WIDTH);
    (*enemies)[enemy_index].entity.size.width += MIN_ENTITY_WIDTH;
    (*enemies)[enemy_index].entity.size.height = xorshift64star_float(MAX_ENTITY_HEIGHT - MIN_ENTITY_HEIGHT);
    (*enemies)[enemy_index].entity.size.height += MIN_ENTITY_HEIGHT;

    struct RGB chroma_key = {.r=255, .g=255, .b=255, .a=0};
    //(*enemies)[enemy_index].entity.tex = CreateTexture("rsc/Spr_SandWitch.bmp", &chroma_key);
    (*enemies)[enemy_index].entity.tex = CreateTexture("rsc/Spr_Quadropus.bmp", &chroma_key);
    (*enemies)[enemy_index].entity.health = xorshift64star_range(MIN_ENTITY_HEALTH, MAX_ENTITY_HEALTH);

    // Enemy starts off motionless
    (*enemies)[enemy_index].entity.physics.accel_x = 0;
    (*enemies)[enemy_index].entity.physics.accel_y = 0;
    (*enemies)[enemy_index].entity.physics.vel_x = 0;
    (*enemies)[enemy_index].entity.physics.vel_y = 0;

    // Place the enemy at some valid location. Enemy must be far enough away from player and not within another enemy
    uint8_t unique_position = 0;
    while(!unique_position)
    {
        unique_position = 1;

        (*enemies)[enemy_index].entity.coords.x_meter = xorshift64star_uniform(SCREEN_WIDTH_METERS);
        (*enemies)[enemy_index].entity.coords.x_meter += ((*enemies)[enemy_index].entity.size.width / 2);

        (*enemies)[enemy_index].entity.coords.y_meter = xorshift64star_uniform(SCREEN_HEIGHT_METERS);
        (*enemies)[enemy_index].entity.coords.y_meter -= ((*enemies)[enemy_index].entity.size.height / 2);

        ValidateEntityWithinBounds( &((*enemies)[enemy_index].entity) );

        Rect new_enemy_rect = GetEntityRectangle( &((*enemies)[enemy_index].entity) );
        
        // Determine if the new enemy is within the player's proximity circle (we don't want enemy spawning too close!)
        // TODO: Do self corrections to attempt to prevent need of randomly selecting new Point
        //          Try to push entity out of proximity circle if possible (while remaining within map bounds)
        //if( RectHasCollidedWithCircle(&new_enemy_rect, &proximity_circle) )
        if( RectCornerWithinCircle(&new_enemy_rect, &proximity_circle) )
        {
            unique_position = 0;
            continue;
        }

        // Gather a list of all the other enemies
        uint32_t num_other_entities = (*num_enemies) - 1;
        uint32_t entity_index = 0;
        Entity* other_entities = malloc(sizeof(Entity) * num_other_entities);
        // Exclude the enemy we are trying to place
        for(int j = 0; j < (*num_enemies) - 1; j++)
        {
            other_entities[entity_index++] = (*enemies)[j].entity;
        }

        // Validate that no enemy overlaps with our new enemy
        ValidateNoEntityOverlap( &(*enemies)[enemy_index].entity, other_entities, num_other_entities );
    }
}

// TODO: Create state variable for last creation frame of enemy, then if say 3 * FPS passed, then create enemy
//          Then update that variable to the new frame.
//          Do I want this? It adds a whole 'nother 64 bit variable and doesn't add that much more value. 
//               It's meaning can be derived with simple modulo arithmetic
static void HandleEnemyUpdates(GameState* state)
{
    double seconds_passed = (state->logical_frames / (double) FRAMES_PER_SECOND);

    // TODO: Figure this spawning frame thing out... Preferably without needing to create another variable in GameState
    // Spawn a new enemy every so many frames depending on difficulty and SPAWN_RATE (+1 to compensate for any div0)
    uint32_t spawn_frame = (FRAMES_PER_SECOND * 5) / (float) SPAWN_RATE;
    spawn_frame += SPAWN_FRAME_BASE;
    spawn_frame -= (30) * state->difficulty;
    //printf("Difficulty = %i\n", state->difficulty);
    //printf("Spawn rate = %f\n", SPAWN_RATE);
    //printf("Spawn frame = %i\n", spawn_frame);
    //printf("FPS / spawn_frame = %f\n", FRAMES_PER_SECOND / spawn_frame);
    if( ( state->logical_frames % spawn_frame ) == 0 )
    {
        /*printf("Time to create an enemy! Spawn frame every %i frames\n", spawn_frame);
        printf("There has been %i frames\n", state->logical_frames);
        printf("This corresponds to %f seconds\n", state->logical_frames / (float) FRAMES_PER_SECOND);*/
        CreateEnemy(state->difficulty, &(state->enemies), &(state->num_enemies), &(state->player));
    }

    // Move all enemies in direction of player
    //     USE PROXIMITY CIRCLE, have enemies move towards proximity circle to be an obstruction
    //     Player is required to dodge enemies and maybe be required to pick up items for points? (to motivate movement)
    Circle proximity_circle = GetProximityCircle(&(state->player), state->difficulty);
    for(int i = 0; i < state->num_enemies; i++)
    {
        Rect enemy_rect = GetEntityRectangle( &(state->enemies[i].entity) );
        // Change the enemy's acceleration direction if they are not within the proximity circle
        if( !RectCornerWithinCircle(&enemy_rect, &proximity_circle) )
        {
            Point nearest_corner_coord;
            enum Quadrant quadrant = GetQuadrantAndNearestRectCornerToCircle(&enemy_rect, 
                                                                        &proximity_circle,  &nearest_corner_coord);
            switch(quadrant)
            {
                // TODO: I currently have difficulty of Demo = 0, this means no acceleration if on demo, do I want this?
                // If quadrant is top left, then we need to move towards bottom right
                case TOP_LEFT:
                {
                    state->enemies[i].entity.physics.accel_x = CONSTANT_ACCEL * state->difficulty;
                    state->enemies[i].entity.physics.accel_y = - CONSTANT_ACCEL * state->difficulty;
                } break;

                // If quadrant is top right, then we need to move towards bottom left
                case TOP_RIGHT:
                {
                    state->enemies[i].entity.physics.accel_x = - CONSTANT_ACCEL * state->difficulty;
                    state->enemies[i].entity.physics.accel_y = - CONSTANT_ACCEL * state->difficulty;
                } break;

                // If quadrant is bottom left, then we need to move towards top right
                case BOTTOM_LEFT:
                {
                    state->enemies[i].entity.physics.accel_x = CONSTANT_ACCEL * state->difficulty;
                    state->enemies[i].entity.physics.accel_y = CONSTANT_ACCEL * state->difficulty;
                } break;

                // If quadrant is bottom right, then we need to move towards top left
                case BOTTOM_RIGHT:
                {
                    state->enemies[i].entity.physics.accel_x = - CONSTANT_ACCEL * state->difficulty;
                    state->enemies[i].entity.physics.accel_y = CONSTANT_ACCEL * state->difficulty;
                } break;
                
                default:
                {
                    printf("ERROR: Retrieval of quadrant and nearest rectangle coordinate to circle failed!\n");
                } break;
            }

        }

        uint32_t num_other_entities = state->num_enemies - 1;
        uint32_t entity_index = 0;
        Entity* other_entities = malloc(sizeof(Entity) * num_other_entities);
        for(int j = 0; j < state->num_enemies; j++)
        {
            if(i != j)
            {
                other_entities[entity_index++] = state->enemies[j].entity;
            }
        }

        ValidateNoEntityOverlap( &state->enemies[i].entity, other_entities, num_other_entities );

        UpdateVelocity( &(state->enemies[i].entity.physics) );
        MoveEntity( &(state->enemies[i].entity) );
    }
}

// Entities are defined with position referring to the center, we want a rectangle defined with x/y being top left
Rect GetEntityRectangle(Entity* entity)
{
    Rect rect;

    rect.width = entity->size.width;
    rect.height = entity->size.height;

    // Top left position of rectangle
    rect.x_meter = entity->coords.x_meter - (rect.width / 2);
    rect.y_meter = entity->coords.y_meter - (rect.height / 2);

    return rect;
}

void InitializeGameState(GameState* state)
{
    state->game_over = 0;
    state->running = 1;
    state->paused = 0;
    state->logical_frames = 0;
    state->fps = FRAMES_PER_SECOND;

    // TODO: Player determined?
    // TODO: Demo is broken.. bad (New enemies accelerate, but none should, so need to ensure the difficulty accel is in update of enemy accel)
    //state->difficulty = Demo; 
    state->difficulty = Easy;
    //state->difficulty = Medium;
    //state->difficulty = Hard;
    //state->difficulty = Impossible;

    state->player.score = 0;
    state->player.entity.health = 0;

    state->player.entity.size.width = xorshift64star_float(MAX_ENTITY_WIDTH - MIN_ENTITY_WIDTH);
    state->player.entity.size.width += MIN_ENTITY_WIDTH;
    state->player.entity.size.height = xorshift64star_float(MAX_ENTITY_HEIGHT - MIN_ENTITY_HEIGHT);
    state->player.entity.size.height += MIN_ENTITY_HEIGHT;

    // Start player in top left corner
    state->player.entity.coords.x_meter = 1 + (state->player.entity.size.width / 2);
    state->player.entity.coords.y_meter = 1 + (state->player.entity.size.height / 2);
    //printf("Start x = %f\nStart y = %f\n", state->player.entity.coords.x_meter, state->player.entity.coords.y_meter);

    state->player.entity.physics.accel_x = 0;
    state->player.entity.physics.accel_y = 0;
    state->player.entity.physics.vel_x = 0;
    state->player.entity.physics.vel_y = 0;

    struct RGB chroma_key = {.r=255, .g=255, .b=255, .a=0};
    state->player.entity.tex = CreateTexture("rsc/Spr_Quadropus.bmp", &chroma_key);
    ScaleTexture(&state->player.entity.tex, (1/(float)2));

    for(int i = 0; i < KeyNum; i++)
    {
        state->player.input.keydown[i] = 0;
    }

    state->enemies = NULL;
    state->num_enemies = 0;

    // TODO: Currently difficulty = Demo creates 0 enemies at start. Is this what I want?
    for(int i = 0; i < INITIAL_ENEMY_COUNT * state->difficulty; i++)
    {
        CreateEnemy(state->difficulty, &(state->enemies), &(state->num_enemies), &(state->player));
    }

    state->obstacles = NULL;
    state->num_obstacles = 0;

}

void UpdateGameState(GameState* state)
{
    state->logical_frames++;
    HandlePlayerMovement(&(state->player));
    HandleEnemyUpdates(state);
    HandleCollisionDetection(state);

    // TODO: End game thingy, restart? prompt? close? show score?
    // TODO: If number of enemies over some cap, then player wins?
    if(state->game_over && state->difficulty)
    {
        printf("GameOver!\n");
        //state->running = 0; // Current just ending the entire game upon death
    }

}

void DestroyGameState(GameState* state)
{
    DestroyTexture(&state->player.entity.tex);
    
    for(int i = 0; i < state->num_enemies; i++)
    {
        DestroyTexture(&state->enemies[i].entity.tex);
    }

    free(state->enemies);
    free(state->obstacles);
}



/*int main()
{
    GameState state;

    InitializeGameState(&state);
    printf("Gamestate Initialized!\n");

    //while(state.running)
    {
        UpdateGameState(&state);
    }

    DestroyGameState(&state);
    printf("Gamestate Destroyed!\n");
}*/




