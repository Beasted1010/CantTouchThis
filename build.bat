

@echo off


cd obj
gcc -c -I../inc ^
       -I "C:\Users\die20\Documents\Code\Libraries\Renderer\inc"^
        ../src/main.c ../src/gamestate.c ../src/xorshiftstar.c

cd ..
gcc -L "C:\Users\die20\Documents\Code\Libraries\Renderer\lib"^
        obj/main.o obj/xorshiftstar.o obj/gamestate.o -lrenderer -lgdi32

