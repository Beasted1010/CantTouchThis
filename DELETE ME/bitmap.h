

#ifndef BITMAP_H
#define BITMAP_H

#include "windows.h"
#include "stdint.h"

// NOTE: These bitmap structures (except for "Bitmap") are not used since Windows defines them
//          They are left here in case I wish to someday move away from Windows entirely.
//          But I am reminded that Bitmap is a Window's format.
typedef struct BitmapFileHeader
{
    uint16_t bfType;
    uint32_t bfSize;
    uint16_t bfReserved1;
    uint16_t bfReserved2;
    uint32_t bfOffBits;
} BitmapFileHeader;

typedef struct BitmapInfoHeader
{
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} BitmapInfoHeader;

typedef struct RGBQuad
{
    uint8_t rgbBlue;
    uint8_t rgbGreen;
    uint8_t rgbRed;
    uint8_t rgbReserved;
} RGBQuad;

typedef struct BitmapInfo
{
    BitmapInfoHeader bmiHeader;
    RGBQuad bmiColors[1];
} BitmapInfo;

typedef struct BMPMask
{
    uint32_t rmask, gmask, bmask, amask;
    uint32_t rshift, gshift, bshift, ashift;
    uint32_t rmax, gmax, bmax, amax;
} BMPMask;

typedef struct Bitmap
{
    // I am using Windows' structures, not the structures above
    BITMAPFILEHEADER bf_info;
    BITMAPINFO* bmi;
    
    uint32_t num_palette_colors;
    uint32_t width, height;
    uint8_t bytes_per_pixel;

    void* pixels;
} Bitmap;


void InitializeBitmap(Bitmap* bitmap, int width, int height);
unsigned char* LoadBitmapImage(const char* file_location, BITMAPINFO** bmi);



#endif //bitmap->H


