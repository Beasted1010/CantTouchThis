

#include "bitmap.h"
#include "common.h"
#include "stdio.h"


void InitializeBitmap(Bitmap* bitmap, int width, int height)
{
    if( bitmap->pixels )
    {
        VirtualFree( bitmap->pixels, 0, MEM_RELEASE );
        free(bitmap->bmi);
    }

    bitmap->width = width;
    bitmap->height = height;

    bitmap->bmi = malloc(sizeof(BITMAPINFO));

    bitmap->bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bitmap->bmi->bmiHeader.biWidth = bitmap->width;
    bitmap->bmi->bmiHeader.biHeight = -bitmap->height; // Negative so bitmap->will be displayed top down (0,0 is top left of screen)
    bitmap->bmi->bmiHeader.biPlanes = 1;
    bitmap->bmi->bmiHeader.biBitCount = 32;
    bitmap->bmi->bmiHeader.biCompression = BI_RGB;
    bitmap->bmi->bmiHeader.biSizeImage = 0;
    bitmap->bmi->bmiHeader.biXPelsPerMeter = 0;
    bitmap->bmi->bmiHeader.biYPelsPerMeter = 0;
    bitmap->bmi->bmiHeader.biClrUsed = 0;
    bitmap->bmi->bmiHeader.biClrImportant = 0;

    bitmap->bytes_per_pixel = bitmap->bmi->bmiHeader.biBitCount / 8;

    // TODO: OTHER STUFF???
    
    uint32_t bitmap_size = (width * height) * (bitmap->bytes_per_pixel); // NumPixels * BytesPerPixel
    bitmap->pixels = VirtualAlloc(0, bitmap_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

    ValidateObject(bitmap->pixels, "bitmap->>pixels");
}

void PrintBitmapFile(FILE* fp)
{
    long int pos = ftell(fp);

    fseek(fp, 0, SEEK_SET);

    int c, i = 0;
    while( (c = fgetc(fp)) != EOF )
    {
        printf("Byte %i = %i\n", i, c);
    }

    fseek(fp, pos, SEEK_SET);
}

// TODO: Broken (shouldn't be such a large number...)
uint32_t GetBitmapPixelDataOffset(FILE* fp)
{
    long int pos = ftell(fp);

    fseek(fp, 10, SEEK_SET);
    uint32_t offset = 0;
    printf("Size of offset = %i bytes\n", sizeof(offset));

    uint8_t val;
    printf("Getting pixel data offset...\n");
    for(int i = 0; i < 4; i++)
    {
        val = fgetc(fp);
        printf("Got int value of %i for byte number %i\n", val, i+1);
        printf("Before bitwise or... offset = %u\n", offset);
        offset = offset | val;
        printf("After bitwise or... offset = %u\n", offset);
        if( i < 3 )
            offset = offset << 8;
    }

    printf("First byte = %u\n", offset >> 24);
    printf("Second byte = %u\n", (offset << 8) >> 24);
    printf("Third byte = %u\n", (offset << 16) >> 24);
    printf("Fourth byte = %u\n", (offset << 24) >> 24 );
    printf("Offset = %u\n", offset);
    
    fseek(fp, pos, SEEK_SET);

    return offset;
}

// TODO: Broken (shouldn't be such a large number...)
uint32_t GetBitmapFileSize(FILE* fp)
{
    long int pos = ftell(fp);

    fseek(fp, 2, SEEK_SET);
    uint32_t file_size = 0;
    printf("Size of file_size = %i bytes\n", sizeof(file_size));

    uint8_t val;
    printf("Getting file size...\n");
    for(int i = 0; i < 4; i++)
    {
        val = fgetc(fp);
        printf("Got int value of %i for byte number %i\n", val, i+1);
        printf("Before bitwise or... file_size = %u\n", file_size);
        file_size = file_size | val;
        printf("After bitwise or... file_size = %u\n", file_size);
        if( i < 3 )
            file_size = file_size << 8;
    }

    printf("First byte = %u\n", file_size >> 24);
    printf("Second byte = %u\n", (file_size << 8) >> 24);
    printf("Third byte = %u\n", (file_size << 16) >> 24);
    printf("Fourth byte = %u\n", (file_size << 24) >> 24 );
    printf("File size = %u\n", file_size);
    
    fseek(fp, pos, SEEK_SET);

    return file_size;
}

void PrintBMFHeader(Bitmap* bitmap)
{
    printf("BITMAP FILE HEADER...\n");
    printf("---------------------------------------------------\n");
    printf("Type... Should be characters \"BM\" for Bitmap file: %c%c\n", (bitmap->bf_info.bfType << 8) >> 8, bitmap->bf_info.bfType >> 8);
    printf("Size of file in bytes: %i\n", bitmap->bf_info.bfSize);
    printf("Reserved1... Unsued, must be 0: %i\n", bitmap->bf_info.bfReserved1);
    printf("Reserved2... Unsued, must be 0: %i\n", bitmap->bf_info.bfReserved2);
    printf("Offset to start of pixel data: %i\n", bitmap->bf_info.bfOffBits);
    printf("---------------------------------------------------\n");
}

void PrintBMIHeader(BITMAPINFOHEADER bmi_header)
{
    printf("BITMAP INFO HEADER FILE...\n");
    printf("---------------------------------------------------\n");
    printf("Size of bitmap header: %i\n", bmi_header.biSize);
    printf("Image width in pixels: %i\n", bmi_header.biWidth);
    printf("Image height in pixels: %i\n", bmi_header.biHeight);
    printf("Number of planes (must be 1): %i\n", bmi_header.biPlanes);
    printf("Bits per pixel: %i\n", bmi_header.biBitCount);
    printf("Compressoin type (0 = uncompressed): %i\n", bmi_header.biCompression);
    printf("Size of image (may be 0 for uncompressed images): %i\n", bmi_header.biSizeImage);
    printf("Preferred resolution in pixels per meter (x dir): %i\n", bmi_header.biXPelsPerMeter);
    printf("Preferred resolution in pixels per meter (y dir): %i\n", bmi_header.biYPelsPerMeter);
    printf("Number color map entries that are actually used: %i\n", bmi_header.biClrUsed);
    printf("Number of significant colors: %i\n", bmi_header.biClrImportant);
    printf("---------------------------------------------------\n");

    if(bmi_header.biSize == 108)
    {
        printf("BITMAP IS V4! IT CONTAINS THE FOLLOWING ADDITIONAL DATA!\n");
        printf("---------------------------------------------------\n");
        //printf("RedMask = %u\n", bmi_header.biRedMask);
    }
}

void PrintRGBQuad(Bitmap* bitmap, uint32_t color_table_size)
{
    printf("---------------------------------------------------\n");
    for(int i = 0; i < color_table_size; i++)
    {
        printf("Table entry #%i: Blue = %i\tGreen = %i\tRed = %i\tReserved = %i\n", i, bitmap->bmi->bmiColors[i].rgbBlue, bitmap->bmi->bmiColors[i].rgbGreen,
                                                                                       bitmap->bmi->bmiColors[i].rgbRed, bitmap->bmi->bmiColors[i].rgbReserved);
    }
    printf("---------------------------------------------------\n");
}

void PrintBitmapInfo(BITMAPINFOHEADER bmi_header)
{
    PrintBMIHeader(bmi_header);
    //PrintRGBQuad(bitmap, bitmap->num_palette_colors);
}

void PrintImageData(unsigned char* image, BITMAPINFO* bmi)
{
    /*uint8_t red = ((image[0] << 0) >> 16);
    uint8_t green = ((image[1] << 8) >> 16);
    uint8_t blue = ((image[2] << 16) >> 16);*/
    uint8_t red = image[0];
    uint8_t green = image[1];
    uint8_t blue = image[2];
    printf("Pixel %i: R = %u\tG = %u\tB = %u\n", 0, red, green, blue);
    printf("image[0] = %u\n", image[0]);
    // ARGB
    // TODO: If there is compression, 16 and 32 bit handling is different
    for(int i = 0; i < bmi->bmiHeader.biSizeImage; i += 3)
    {
        getchar();
        switch(bmi->bmiHeader.biBitCount)
        {
            case 32:
            {
                // For 32 bit pixels
                uint8_t alpha;
                uint8_t red;
                uint8_t green;
                uint8_t blue;
                
                if(bmi->bmiHeader.biCompression = 3)
                {
                    BMPMask bit_mask;
                    if(bmi->bmiHeader.biSize == 40) // Version 3
                    {
                        bit_mask.rmask = 0xFFC00000;
                        bit_mask.gmask = 0x003FF000;
                        bit_mask.bmask = 0x00000FFC;
                    }
                    else // Version 4 or later
                    {
                        bit_mask.amask = 0xFF000000;
                        bit_mask.rmask = 0x00FF0000;
                        bit_mask.gmask = 0x0000FF00;
                        bit_mask.bmask = 0x000000FF;
                        alpha = image[i] && bit_mask.amask;
                    }
                    uint32_t pixel = 0;
                    pixel |= image[i++]; pixel <<= 8;
                    pixel |= image[i++]; pixel <<= 8;
                    pixel |= image[i++]; pixel <<= 8;
                    pixel |= image[i++];

                    alpha = pixel >> 24; //&& bit_mask.amask;
                    red = (pixel << 8) >> 24;// && bit_mask.rmask;
                    green = (pixel << 16) >> 24;// && bit_mask.gmask;
                    blue = (pixel << 24) >> 24;// && bit_mask.bmask;
                }
                else
                {
                    alpha = image[i];
                    red = image[i+1];
                    green = image[i+2];
                    blue = image[i+3];
                }
    
                printf("Pixel %i: A = %u\tR = %u\tG = %u\tB = %u\n", i+1, alpha, red, green, blue);
            } break;

            case 24:
            {
                // For 24 bit pixels
                red = image[i];
                green = image[i+1];
                blue = image[i+2];
                printf("Pixel %i: R = %u\tG = %u\tB = %u\n", i+1, red, green, blue);
            } break;

            case 16:
            {
                // For 16 bit pixels
                uint8_t alpha;
                uint8_t red;
                uint8_t green;
                uint8_t blue;
                
                if(bmi->bmiHeader.biCompression = 3)
                {
                    BMPMask bit_mask;
                    if(bmi->bmiHeader.biSize == 40) // Version 3
                    {
                        bit_mask.rmask = 0xF8000000;
                        bit_mask.gmask = 0x07E00000;
                        bit_mask.bmask = 0x001F0000;
                    }
                    else // Version 4
                    {
                        bit_mask.amask = 0xF8000000;
                        bit_mask.rmask = 0x07C00000;
                        bit_mask.gmask = 0x003E0000;
                        bit_mask.bmask = 0x001F0000;
                        alpha = image[i] && bit_mask.amask;
                    }

                    red = image[i] && bit_mask.rmask;
                    green = image[i+1] && bit_mask.gmask;
                    blue = image[i+2] && bit_mask.bmask;
                }
                else
                {
                    red = image[i];
                    green = image[i+1];
                    blue = image[i+2];
                }

                printf("Pixel %i: R = %u\tG = %u\tB = %u\n", i+1, red, green, blue);
            } break;

            case 8:
            {
                // For 8 bit pixels
                uint8_t alpha;
                uint8_t red;
                uint8_t green;
                uint8_t blue;

                alpha = (image[i] >> 24);
                red = image[i];
                green = image[i+1];
                blue = image[i+2];

                if(bmi->bmiHeader.biClrUsed)
                {
                    alpha = bmi->bmiColors[image[i]].rgbReserved;
                    red = bmi->bmiColors[image[i]].rgbRed;
                    green = bmi->bmiColors[image[i]].rgbGreen;
                    blue = bmi->bmiColors[image[i]].rgbBlue;
                }

                printf("Pixel %i: A = %u\tR = %u\tG = %u\tB = %u\n", i+1, alpha, red, green, blue);
            } break;

            default:
            {
                printf("The bitmap type is not supported!\n");
            } break;
        }
    }
}

static inline void SwapBitPlacement(unsigned char* image, int width, int height, uint8_t bpp, uint32_t color_used)
{
    // If height > 0 then the image is bottom-up. Our bitmap assumes top down, so adjust for the image
    int image_index = (height-1) * width * (bpp / 8);
    int index_adjust = 2 * -width * (bpp / 8);
    int byte_swapper;
    int left_counter = 0;

    RGB rgb = {.a=0, .r=0, .g=0, .b=0};
    // TODO: Probably should do all this bit switching at time of initial image reading? That way we don't keep recalculating all this....
    for(int image_row = 0; image_row < height / 2; image_row++)
    {
        for(int image_col = 0; image_col < width; image_col++)
        {
            if(color_used) // Assuming 1 byte per pixel ... TODO: Ensure this to be the case
            {
                byte_swapper = image[left_counter];
                image[left_counter++] = image[image_index];
                image[image_index++] = byte_swapper;
            }
            else // Assuming 3 bytes per pixel ... TODO: Ensure this to be the case?
            {
                // Do this swapping for the next 3 bytes
                for(int k = 0; k < 3; k++)
                {
                    byte_swapper = image[left_counter];
                    image[left_counter++] = image[image_index];
                    image[image_index++] = byte_swapper;
                }
            }

            //printf("a = %i\tr = %i\tg = %i\tb = %i\n", rgb.a, rgb.r, rgb.g, rgb.b);
            //getchar();
        }
        image_index += index_adjust;
    }
}

unsigned char* LoadBitmapImage(const char* file_location, BITMAPINFO** bitmap_info)
{
    BITMAPFILEHEADER bitmap_file_header;
    unsigned char* bitmap_image;
    int image_index = 0; // Image index counter
    unsigned char tempRGB; // Swap variable

    FILE* fp = fopen(file_location, "rb");
    ValidateObject(fp, "Bitmap image file pointer");

    fread(&bitmap_file_header, 1, sizeof(BITMAPFILEHEADER), fp); // NOTE: This is correct (verified with print out)
    printf("BITMAPFILEHEADER = %i\tfp = %i\n", sizeof(BITMAPFILEHEADER), fp);

    // File must be a bitmap -> "BM"
    if(bitmap_file_header.bfType !=0x4D42)
    {
        printf("This file is not a bitmap file!\n");
        fclose(fp);
        return NULL;
    }

    if(!(*bitmap_info))
    {
        printf("Bitmap_info hasn't been allocated memory yet! Allocating memory for it now...\n");
        (*bitmap_info) = (BITMAPINFO*) malloc(sizeof((*bitmap_info)->bmiHeader) + sizeof(RGBQUAD));
        ValidateObject(*bitmap_info, "bitmap_info");
    }

    fread(&((*bitmap_info)->bmiHeader), 1, sizeof((*bitmap_info)->bmiHeader), fp); // NOTE: This is correct (verified with print out)
    printf("BITMAPINFO= %i\tfp = %i\n", sizeof(BITMAPINFO), fp);

    uint8_t is_v4 = ((*bitmap_info)->bmiHeader.biSize > 40);

    BMPMask bit_mask;
    if((*bitmap_info)->bmiHeader.biCompression == 3)
    {
        uint32_t mask_size;
        if(is_v4)
        {
            mask_size = 4 * sizeof(DWORD);
        }
        else
        {
            mask_size = 3 * sizeof(DWORD);
        }

        fread(&(bit_mask.rmask), 1, sizeof(mask_size), fp);
        fread(&(bit_mask.gmask), 1, sizeof(mask_size), fp);
        fread(&(bit_mask.bmask), 1, sizeof(mask_size), fp);

        bit_mask.rshift = __builtin_ffs(bit_mask.rmask) - 1;
        bit_mask.gshift = __builtin_ffs(bit_mask.gmask) - 1;
        bit_mask.bshift = __builtin_ffs(bit_mask.bmask) - 1;

        bit_mask.rmax = bit_mask.rmask >> bit_mask.rshift;
        bit_mask.gmax = bit_mask.gmask >> bit_mask.gshift;
        bit_mask.bmax = bit_mask.bmask >> bit_mask.bshift;

        if(is_v4)
        {
            fread(&(bit_mask.amask), 1, sizeof(mask_size), fp);
            bit_mask.ashift = __builtin_ffs(bit_mask.amask) - 1;
            bit_mask.amax = bit_mask.amask >> bit_mask.ashift;
        }
        else
        {
            bit_mask.amask = 0;
            bit_mask.ashift = 0;
            bit_mask.amax = 0;
        }
    }

    if((*bitmap_info)->bmiHeader.biClrUsed)
    {
        if((*bitmap_info)->bmiHeader.biBitCount < 16)
        {
            // TODO: Number of color palette entries = maximum size possible for the colormap -> Do I need to handle this? How?
        }
        uint32_t num_palette_colors = (*bitmap_info)->bmiHeader.biClrUsed;
        (*bitmap_info) = (BITMAPINFO*) realloc((*bitmap_info), sizeof(BITMAPINFO) + (sizeof(RGBQUAD) * num_palette_colors));

        fread(&((*bitmap_info)->bmiColors), 1, sizeof(RGBQUAD) * num_palette_colors, fp);
    }

    if((*bitmap_info)->bmiHeader.biSize == 108)
    {
        printf("Bitmap is Version 4! is_v4 = %i\n", is_v4);
    }
    else if((*bitmap_info)->bmiHeader.biSize == 40)
    {
        printf("Bitmap is Version 3! is_v4 = %i\n", is_v4);
        
    }
    else
    {
        printf("Bitmap is some unsupported version!\n");
    }

    PrintBMIHeader(((*bitmap_info)->bmiHeader));

    printf("biSizeImage = %i\n", (*bitmap_info)->bmiHeader.biSizeImage);
    bitmap_image = (unsigned char*) malloc( sizeof(unsigned char) * (*bitmap_info)->bmiHeader.biSizeImage );

    if(!bitmap_image)
    {
        printf("Failed to allocate bitmap memory!\n");
        fclose(fp);
        return NULL;
    }

    // Move file pointer to beginning of bitmap data (where the image data is located)
    if(fseek(fp, bitmap_file_header.bfOffBits, SEEK_SET) != 0) printf("ERROR SEEKING TO IMAGE BITS!\n");
    else fread(bitmap_image, 1, (*bitmap_info)->bmiHeader.biSizeImage, fp);

    RGB colors;
    // Swap the r and b values to get RGB (image is BGR)?
    for(image_index = 0; image_index < (*bitmap_info)->bmiHeader.biSizeImage;)
    {
        if((*bitmap_info)->bmiHeader.biBitCount == 24)
        {
            tempRGB = bitmap_image[image_index];
            bitmap_image[image_index] = bitmap_image[image_index + 2];
            bitmap_image[image_index + 2] = tempRGB;
            image_index += 3;
        }
        else if((*bitmap_info)->bmiHeader.biBitCount == 32)
        {
            image_index++;
            /*tempRGB = bitmap_image[image_index];
            bitmap_image[image_index] = bitmap_image[image_index + 2];
            bitmap_image[image_index + 2] = tempRGB;*/
            image_index += 3;
        }
    }

    //PrintImageData(bitmap_image, (*bitmap_info));

    // If height > 0 then the image is bottom-up. Our bitmap assumes top down, so adjust for the image
    if((*bitmap_info)->bmiHeader.biHeight > 0)
    {
        SwapBitPlacement(bitmap_image, (*bitmap_info)->bmiHeader.biWidth, (*bitmap_info)->bmiHeader.biHeight, (*bitmap_info)->bmiHeader.biBitCount, (*bitmap_info)->bmiHeader.biClrUsed);
    }

    //PrintBitmapFile(fp);
    //GetBitmapPixelDataOffset(fp);
    
    fclose(fp);
    return bitmap_image;
}








